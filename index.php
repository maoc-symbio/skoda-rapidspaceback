<?php

	ini_set('date.timezone', 'Europe/Berlin');

	require_once __DIR__.'/vendor/autoload.php';
	require_once __DIR__.'/app/config.php';

	define('WWW_DIR', __DIR__);
	define('APP_DIR', __DIR__ . '/app');
	define('ASSETS_DIR', __DIR__ . '/assets');
	define('LOG_DIR', __DIR__ . '/log');
	define('CACHE_DIR', __DIR__ . '/cache');

	$app = new Silex\Application();

	switch ($_SERVER['HTTP_HOST'])
	{
		case 'maoc.azurewebsites.net':
			$app['config'] = $config['test'];
		break;

		case 'microsite-hyperlapse.skoda.jiri.dev.symbio.intra':
			$app['config'] = $config['dev_rambo'];
		break;

		case 'pu170.symbio.cz':
			$app['config'] = $config['test'];
		break;

		case 'rapidspaceback.com':
        case 'www.rapidspaceback.com':
        case 'skdarpdspcbckhprlps.azurewebsites.net':
        case 'skoda-rapid-spaceback-hyperlapse.azurewebsites.net':
			$app['config'] = $config['production'];
		break;

		default:
			$app['config'] = $config['dev'];
		break;
	}

	$app['config'] = array_merge($app['config'], $config['application']);
	$app['debug'] = $app['config']['debug'];

	require_once APP_DIR . '/bootstrap.php';

	$app->run();