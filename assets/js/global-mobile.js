/*!
 *
 *  Project:  Skoda Rapid Spaceback
 *  Author:   Petr Urbanek - www.r4ms3s.cz
 *  Twitter:  @r4ms3scz
 *
 * @param {Object} window, document, undefined
 *
 */

 (function(window, document, undefined) {

    // Defaults
    // =====================================

    var r4 = window.r4 = {
        utils : {},
        cache : {},
        actions : {},
    };

    // Methods
    // =====================================

    r4.utils.init = function() {
        r4.cache.window                = $(window);
        r4.cache.document              = $(document);
        r4.cache.html                  = $('html');
        r4.cache.body                  = $('body');

        r4.cache.page                  = r4.cache.body.find('.page');
        r4.cache.header                = r4.cache.body.find('header');
        r4.cache.footer                = r4.cache.body.find('footer');
        r4.cache.section               = r4.cache.body.find('section');
    };

    // RESIZE
    r4.utils.resize = function(start){
        //default 640x960
        if($(window).width() < $(window).height()) {
            $('body').css({
              'font-size': 20 / (640 / $(window).width()) + 'px',
              'line-height:': 20 / (640 / $(window).width()) + 'px'
            });
        } else {
            $('body').css({
              'font-size': 20 / (960 / $(window).width()) + 'px',
              'line-height:': 20 / (960 / $(window).width()) + 'px'
            });
        }
    };

    r4.utils.domLoad = function() {
        $('#link-to-bottom-links').on('click', function(e){
            $('#bottom-links, #link-to-bottom-links').toggleClass('bottom-link-displayed');

            e.preventDefault();
        });

        $('#link-to-music').on('click', function(e){
            $('#music-links, #link-to-music-links').toggleClass('music-link-displayed');

            e.preventDefault();
        });

        $('#link-language').on('click', function(e){
            $('html, body, #link-language, #languages').toggleClass('language-displayed');

            e.preventDefault();
        });

        $('.btn-privacy-policy, .privacy-policy .btn-close').on('click', function(e){
            $('html, body, .privacy-policy').toggleClass('popup-displayed');

            e.preventDefault();
        });

        var page = $('body').data('page');
        if (r4.actions[page] !== undefined) {
            r4.actions[page]();
        }

        $('.ul-list').on('click', 'li', function(e){
            location.href = '/' + appConfig.culture + '/m/create?r=' + $(this).data('route-id');

            e.preventDefault();
        });

        var a = document.createElement('audio');
        if((!!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''))) || (!!(a.canPlayType && a.canPlayType('audio/wav; codecs="1"').replace(/no/, '')))) {
            var video = document.getElementById('video-player');

            if(video) {
                $('.skoda-play').on('click', function(e){
                    $('#video-player-wrap').addClass('is-played');

                    video.play();

                    e.stopPropagation();
                });

                $('#video-player-wrap-close').on('click', function(e){
                    $('#video-player-wrap').removeClass('is-played');

                    video.pause();

                    e.stopPropagation();
                });

                video.addEventListener('ended', videoEnded, false);
                function videoEnded(e) {
                    if(!e) { e = window.event; }
                    $('#video-player-wrap').removeClass('is-played');
                }

                video.addEventListener('webkitendfullscreen', videoExitFullscreen, false);
                function videoExitFullscreen(e) {
                    if(!e) { e = window.event; }
                    $('#video-player-wrap').removeClass('is-played');
                }
            }
        } else {
            $('.skoda-play').addClass('no-video');
        }

        // RESIZE
        r4.utils.resize();
        r4.cache.window.smartresize(function() {
            r4.utils.resize();
        });
    };

    function getRouteList(callback) {
        var routeList = [],
            template;

        $.ajax({
            type: 'GET',
            url: '/assets/xml/config.xml',
            cache: false,
            dataType: 'xml',
            success: function(xml) {
                $(xml).find('routes route').each(function(i, el) {
                    template = SkodaBackendConfig.Templates.ride;
                    template = template
                                .replace(/__routeName__/, SkodaBackendConfig.i18n['route_' + (i+1)])
                                .replace(/__image__/, '/assets/img/routes/' + $(this).find('image').text())
                                .replace(/__routeId__/, i);
                    routeList.push(template);
                });
                return callback(routeList);
            }
        });
    }

    r4.actions.journey = function() {
        getRouteList(function(list) {
            $('.ul-list').html(list.join(''));
        });
    };

    r4.playJourney = function() {
        var route;

        if (appConfig.route >= 0)
        {
            $.ajax({
                type: 'GET',
                url: '/assets/xml/config.xml',
                cache: false,
                dataType: 'xml',
                success: function(xml) {
                    $(xml).find('routes route').each(function(i, el) {
                        if (i == appConfig.route) {
                            route = $(this);
                        }
                    });

                    if(!route) { return; }

                    if (route.find('gps from lat').length > 0)
                    {
                        var gps = {
                            fromLat : parseFloat(route.find('gps from lat').text()),
                            fromLng : parseFloat(route.find('gps from lng').text()),
                            toLat : parseFloat(route.find('gps to lat').text()),
                            toLng : parseFloat(route.find('gps to lng').text())
                        };

                        // Call skoda maps direction service
                        SkodaMap.getDirections({
                            from: [gps.fromLat, gps.fromLng],
                            to: [gps.toLat, gps.toLng],
                            target: [0, 0],
                            pitch: 0,
                            effectID: 0,
                            speed: 7,
                            sound: true
                        });
                    }
                    else
                    {
                        var points = route.find('points point'),
                            pl = points.length,
                            gps = {},
                            _p,
                            routePoints = [];

                        for (var i=0,l=pl;i<l;i++) {
                            _p = points[i];
                            pt = $(_p).text().split(',');
                            routePoints[i] = [parseFloat(pt[0]), parseFloat(pt[1])];
                        }

                        SkodaStreetView.preload(routePoints, [0,0], 7, 0, 0);

                        if(!appConfig.isTablet) {
                            SkodaMap.dispose();
                        }
                    }

                    //SkodaStreetView.initAudio(-1);
                }
            });
        }
    };

    r4.actions['after-video'] = function() {
        getRouteList(function(list) {
            $('.ul-list').html(list.join(''));

            $('#another-video .count').text(list.length);

            var $frame = $('.sly');
            var $wrap  = $frame.parent();

            $frame.find('li').css({
                'border-width': '0 ' + $(window).width() / 100 * 5 + 'px',
                'width': $(window).width()
            });

            var option = {
                horizontal: 1,
                itemNav: 'basic',
                itemNav: 'forceCentered',
                activateMiddle: 1,
                smart: 1,
                activateOn: 'click',
                activatePageOn: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1
            };

            $frame.sly(option, {
                load: function() {
                },
                moveStart: function() {
                },
                moveEnd: function() {
                    $('#another-video .actual').text($frame.find('.active').prevAll().length + 1);
                    //alert($frame.find('ul').position().left)
                    //alert($frame.find('.active').prevAll().length);
                }
            });
        });
    };


    // Initialize Events
    // =====================================

    r4.utils.init();

    jQuery(function($) {
        r4.utils.domLoad();
    });


})(window, document);

// debouncing function from John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
(function($,sr) {

    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap) {
                    func.apply(obj, args);
                }
                timeout = null;
            }

            if (timeout) {
                clearTimeout(timeout);
            } else if (execAsap) {
                func.apply(obj, args);
            }
            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    jQuery.fn[sr] = function(fn){ return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
