(function() {

    var Loader = function(){
        var loader = {
            id: null,
            image: null,
            callback: null
        };

        loader.start = function(index, url, cb){
            loader.id = index;
            loader.callback = cb;
            loader.image = new Image();
            loader.image.onload = function(){
                loader.callback(loader, this);
            }
            loader.image.src = url;
        }

        loader.stop = function(){
            if(loader.image){
                loader.image.onload = null;
                loader.image.removeAttribute('src');
            }
            loader.id = null;
            loader.image = null;
            loader.callback = null;
        }

        return loader;

    };

    var SV = function(xml){

        if(window.SkodaStreetView){
            window.SkodaStreetView = window.SkodaStreetView;

            alert("SkodaStreetView is already running!");
            return {
                initAudio: window.SkodaStreetView.initAudio,
                preload: window.SkodaStreetView.preload,
                dispose: window.SkodaStreetView.dispose
            };
        }

        var FPS = 80,
            STATUS = 0,
            _config = xml,
            _bitmaps = null,
            _images = null,
            _imagesLoaded = 0,
            _loadingIndex = 0,
            _activeLoaders = null,
            _imagesTotal = 0,
            _preloaderTimeout = null,

            _isTouchSupported = false,
            _startEvent = "mousedown",
            _moveEvent = "mousemove",
            _endEvent = "mouseup",
            _cancelEvent = null,

            IMAGE_WIDTH = 1000,
            IMAGE_HEIGHT = 400,
            SCREEN_WIDTH = 0,
            SCREEN_HEIGHT = 0,



            _timeline = null,

            _prevImg = null,
            _currentImg = null;

        init();

        function init(){

            _isTouchSupported = 'ontouchstart' in window.document;
            if(_isTouchSupported){
                _startEvent = "touchstart";
                _moveEvent = "touchmove";
                _endEvent = "touchend";
                _cancelEvent = "touchcancel";
            }

            _timeline = $("#timeline");
            _timelineContainer = $("#timeline-container");



            //window.onresize = function() { SkodaStreetView.resize(); }
            window.addEventListener('resize', resize, false);
            resize();
        }

        function preload(route, target, speed, pitch, effectID){

            STATUS = 1;

            var transitionTime = 1000 / speed;
            var transitionFrames = transitionTime / 33;
            _alphaStep = 1 / transitionFrames;

            _imagesLoaded = 0;
            _imagesTotal = route.length;
            _images = [];

            var hasTarget = !(target[0] == 0 && target[1] == 0),
                url = "",
                angle = 0;


            //console.log(_imagesTotal + ' a ' + _imagesTotal / 3);

            for(var i = 0; i < _imagesTotal; i+=3){
                //console.log(i);

                if(hasTarget){
                    angle = calculateAngleBetween(route[i], target);
                }else{
                    if (i == _imagesTotal - 1) {
                        angle = calculateAngleBetween(route[_imagesTotal - 2], route[_imagesTotal - 1]);
                    } else {
                        angle = calculateAngleBetween(route[i], route[i + 1]);
                    }
                }

                url = '/assets/image.php?params[size]=' + IMAGE_WIDTH + 'x' + IMAGE_HEIGHT +
                    '&params[location]=' + route[i][0] + ',' + route[i][1] +
                    '&params[fov]=90&params[heading]=' + angle +
                    '&params[pitch]=' + pitch + '&params[mobile]=1';

                _images.push(url);
            }

            _timelineContainer.show();

            $('#sv-overlay').show();
            $('html, body').addClass('hidden');
            $('#sv-preloader, #sv-overlay').show();
            $('#sv-progress').width('1%');
            $('.maps-aside, .maps-help, #map-container').hide();


            _loadingIndex = 0;
            _activeLoaders = [];

            for(var i = 0; i < 10; i++){
                if(_images.length < 1) break;

                var l = new Loader();
                l.start(_loadingIndex, _images[0], onImageLoaded);
                _activeLoaders.push(l);

                _images.splice(0, 1);
                _loadingIndex++;
            }

            changePreloader();

        }

        var _preloaderIndex = 0;

        function changePreloader(){

            var key = "fl_largeText" + _preloaderIndex;
            if(!SkodaBackendConfig.i18n[key]){
                _preloaderIndex = 0;
                key = "fl_largeText" + _preloaderIndex;
            }

            $("#preloader-large-text").text(SkodaBackendConfig.i18n[key]);

            key = "fl_smallText" + _preloaderIndex;
            var small = SkodaBackendConfig.i18n[key];
            var sParts = small.split("<br />")
            $("#preloader-small-text1").text(sParts[0]);
            $("#preloader-small-text2").text(sParts[1]);

            _preloaderIndex++;
            _preloaderTimeout = window.setTimeout(changePreloader, 3000);
        }

        function onImageLoaded(loader, image){

            if(!_bitmaps) _bitmaps = [];

            _imagesLoaded++;
            var p = Math.floor((_imagesLoaded / (_imagesTotal / 3) ) * 100);
            $('#sv-progress').width(p + '%');

            _bitmaps[loader.id] = image;

            for(var i = 0; i < _activeLoaders.length; i++){
                if(loader == _activeLoaders[i]){
                    _activeLoaders.splice(i, 1);
                    break;
                }
            }

            if(_images.length > 0){
                var l = new Loader();
                l.start(_loadingIndex, _images[0], onImageLoaded);
                _activeLoaders.push(l);

                _images.splice(0, 1);
                _loadingIndex++;

            }

            if(_imagesLoaded == + Math.ceil(_imagesTotal / 3)){

                for(var i = 0; i < _bitmaps.length; i++){
                    _timeline.append(_bitmaps[i]);
                }
                _bitmaps = null;

                play();
            }


        }

        function play(){

            STATUS = 2;

            _time = new Date().getTime();

            if(_preloaderTimeout)
                window.clearTimeout(_preloaderTimeout);
            _preloaderTimeout = null;

            resize();
            playAudio();

            $('html, body').removeClass('hidden');
            $('.create-header, #map-tutorial, #sv-overlay').hide();

            _transition = 0;
            _onEnterFrame = window.setInterval(enterFrame, FPS);

            if(!appConfig.isTablet) {
              $('#create-content').on(_startEvent, handleMouseDown);

                if(_isTouchSupported){
                    $('#create-content').on(_moveEvent, handleMouseMove);
                    $('#create-content').on(_endEvent, handleMouseUp);
                    $('#create-content').on(_cancelEvent, handleMouseCancel);
                }
            }

            var back = $("#sv-back-button");
            back.off(_startEvent);
            back.on(_startEvent, function(event){
                event.preventDefault();
                event.stopPropagation();

                dispose();

                if(!window.SkodaMap){
                    window.SkodaMap = new SkodaMapModule();
                }
            });
        }

        var _onEnterFrame = null,
            _time = 0,
            _frame = 1,
            _transition = 0,
            _alpha = 0,
            _alphaStep = 1;

        function enterFrame(){

            var child;

            if(_transition == 1){
                child = _timeline.children().get(1);

                if(!$(child).hasClass("alpha_image")){
                    $(child).addClass("alpha_image");
                    _frame++;
                }

                if(_alpha < 0.95){

                    _alpha += _alphaStep;
                    $(child).css({ opacity: _alpha });

                    _transition = 0;
                }else{
                    _alpha = 0;
                    _transition = 2;
                }

            }

            if(_transition == 2){

                child = _timeline.children().get(1);
                $(child).removeClass("alpha_image");

                child = _timeline.children().get(0);
                $(child).remove().end();
                _timeline.append(child);

                _transition = 0;

            }

            _transition++;

            if(_frame >= _imagesLoaded && _alpha == 0){
                var now = new Date().getTime();
                var diff = (now - _time) / 1000;

                if(diff < 15){
                    _frame = 0;
                }else{
                    // TODO: window.loaction

                    if(!appConfig.isTablet) {
                        window.location.href = document.URL.replace('/create', '/after-video');
                    } else {
                        Skoda.Router.navigate(4);
                    }

                    dispose();
                }
            }

        }

        /*************************************
        * STREETVIEW DRAGGING
        *************************************/

        var _startDragPoint,
            _timelineOffset;


        function handleMouseDown(event) {

            var eventObj = _isTouchSupported ? event.originalEvent.touches[0] : event;
            event.preventDefault();
            event.stopPropagation();

            var tlPos = _timelineContainer.position();

            _startDragPoint = { x: eventObj.pageX,
                                y: eventObj.pageY,
                                top: tlPos.top,
                                left: tlPos.left
                            };

            if(!appConfig.isTablet) {
                if(!_isTouchSupported){
                    $(document).on(_moveEvent, handleMouseMove);
                    $(document).on(_endEvent, handleMouseUp);
                }
            }

        }

        function handleMouseMove(event) {

            var eventObj = _isTouchSupported ? event.originalEvent.touches[0] : event;
            event.preventDefault();
            event.stopPropagation();

            var distX = (_startDragPoint.x - eventObj.pageX);
            var distY = (_startDragPoint.y - eventObj.pageY);
            var x = _startDragPoint.left - distX;
            var y = _startDragPoint.top - distY;
            if(x > 0) x = "0px";
            if(x < _timelineOffset.x) x = _timelineOffset.x + "px";
            if(y > 0) y = "0px";
            if(y < _timelineOffset.y) y = _timelineOffset.y + "px";

            _timelineContainer.css({ left: x + "px", top: y + "px" });

        }


        function handleMouseUp(event) {
            if(!_isTouchSupported){
                $(document).off(_moveEvent);
                $(document).off(_endEvent);
            }
        }

        function handleMouseCancel(event) {
            handleMouseUp();
        }

        /*************************************
        * AUDIO
        *************************************/

        var _audio = null,
            _audioInited = false;

        function initAudio(id){

            if(id == -1){
                var len = $(_config).find('moods mood').length;
                id = Math.floor((Math.random()*len));
            }

            var pathMp3, pathOgg;
            $(_config).find('moods mood').each(function(){
                var node = $(this);
                if(node.attr("id") == id){
                    pathMp3 = node.find("mp3").text();
                    pathOgg = node.find("ogg").text();
                    return;
                }
            });

            if(_audioInited) return;
            _audioInited = true;


            _audio = document.createElement('audio');

            var source1 = document.createElement('source');
            source1.type= 'audio/ogg';
            source1.src= pathOgg;
            _audio.appendChild(source1);

            var source2 = document.createElement('source');
            source2.type= 'audio/mpeg';
            source2.src= pathMp3;
            _audio.appendChild(source2);
            
            //Map.audio.addEventListener('ended', Map.repeatAudio, false);
            _audio.volume = 0;
            _audio.play();
            _audio.pause();
        }

        function playAudio(){
            if(!_audio) return;

            _audio.volume = 1;
            _audio.currentTime = 0;
            if(_audio.paused){
              _audio.play();
            }
        }

        function disposeAudio(){
            if(!_audio) return;

            _audio.pause();
            delete _audio;
        }

        /*************************************
        * HELPERS
        *************************************/

        function calculateAngleBetween(pointA, pointB){
            return Math.atan2(pointB[1] - pointA[1], pointB[0] - pointA[0]) * 180 / Math.PI;
        }

        function resize(force){
            if(STATUS < 1) return;

            if(!force && SCREEN_WIDTH == document.body.offsetWidth && SCREEN_HEIGHT == document.body.offsetHeight){
                return;
            }

            SCREEN_WIDTH = $(window).width();
            SCREEN_HEIGHT = $(window).height();

            var scale = SCREEN_HEIGHT / IMAGE_HEIGHT;
            if(scale < 1) scale = 1;
            var w = IMAGE_WIDTH * scale;
            var h = SCREEN_HEIGHT;

            if(w < SCREEN_WIDTH){
                scale = SCREEN_WIDTH / IMAGE_WIDTH;
                w = SCREEN_WIDTH;
                h = IMAGE_HEIGHT * scale;
            }

            var left = Math.round((SCREEN_WIDTH - w) / 2) + "px";
            var top = Math.round((SCREEN_HEIGHT - h) / 2) + "px";

            _timelineOffset = { x: SCREEN_WIDTH - w, y: SCREEN_HEIGHT - h };

            if(!appConfig.isTablet) {
                _timelineContainer.css({ width: w + 'px', height: h + 'px', top: top, left: left });
            } else {
                _timeline.find('img').css({
                    'height': $(window).height()
                });

                _timeline.find('img').css({
                    'margin-left': - (_timeline.find('img:eq(0)').width() - $(window).width()) / 2
                });
            }            
        }

        function dispose(){

            if(STATUS < 1) return;

            if(STATUS == 1) {

                while(_activeLoaders.length > 0){
                    _activeLoaders[0].stop();
                    _activeLoaders.splice(0, 1);
                }

            } else if(STATUS == 2) {



            }

            if(_preloaderTimeout)
                window.clearTimeout(_preloaderTimeout);
            delete _preloaderTimeout;

            disposeAudio();

            $(document).off(_startEvent);
            $(document).off(_moveEvent);
            $(document).off(_endEvent);
            if(_cancelEvent)
                $(document).off(_cancelEvent);

            if(_onEnterFrame){
                window.clearInterval(_onEnterFrame);
                delete _onEnterFrame;
            }

            _timelineContainer.hide();

            /*
            $('#sv-overlay').hide();
            $('#sv-preloader').hide();
            */

            STATUS = 0;

            _timeline.empty();
            delete _timeline;
            delete _timelineContainer;

            delete window.SkodaStreetView;


            /*
            // TODO: stop loading, clear imeline children, clear audio
            // clear variables
            */
        }

        return {
            initAudio: initAudio,
            preload: preload,
            dispose: dispose
        };

    };

    window.SkodaStreetViewModule = SV;

})();

//window.onload = function() {
    //new SkodaStreetViewModule();
    //new SkodaStreetViewModule();
//}