// --------------------------------------------------------------------------
// Skoda application
// --------------------------------------------------------------------------

var Skoda = (function($, r4, SkodaBackendConfig)
{
  // --------------------------------------------------------------------------
  // Templates
  // --------------------------------------------------------------------------

  var Templates = {};
  for (var key in SkodaBackendConfig.Templates) {
    Templates[key] = SkodaBackendConfig.Templates[key];
  }

  // --------------------------------------------------------------------------
  // DOM helper
  // --------------------------------------------------------------------------

  var DOMHelper = (function() {

    var routeListArray = [];

    $.ajax({
      type: 'GET',
      url: '/assets/xml/config.xml',
      cache: false,
      dataType: 'xml',
      success: prepareRouteList
    });

    function prepareRouteList(xml)
    {
      var defaults = { speed: 10, pitch: 0 },
        routeOptions = ['name', 'speed', 'pitch', 'image', 'effect', 'gps'],
        i = 0,
        j = 0,
        l;

      $(xml).find('routes route').each(prepareRoutes);

      $('.page-2 .ul-list').html(DOMHelper.getRoutesHTML('routes'));

      function prepareRoutes()
      {
        var route = $(this), topBottom, _r = {};
        for (j=0,l=routeOptions.length;j<l;j++) {
          _r[routeOptions[j]] = route.find(routeOptions[j]).text();
        }
        _r.points = route.find('points');

        var routeObject = {
          id: i,
          name: _r.name,
          speed: _r.speed.length ? parseFloat(_r.speed) : defaults.speed,
          pitch: _r.pitch.length ? parseFloat(_r.pitch) : defaults.pitch,
          image: _r.image,
          effect: parseFloat(_r.effect),
          GPS: _r.gps.length ? true : false,
          points: _r.points.text().length ? true : false
        };

        if (routeObject.GPS)
        {
          routeObject.GPS = {
            fromLat : parseFloat(route.find('gps from lat').text()),
            fromLng : parseFloat(route.find('gps from lng').text()),
            toLat : parseFloat(route.find('gps to lat').text()),
            toLng : parseFloat(route.find('gps to lng').text()),
            target : route.find('gps from target').length ? parseFloat(route.find('gps from target').text()) : [0, 0]
          };
        }

        if (routeObject.points)
        {
          routeObject.points = [];
          _r.points.find('point').each(function() {
            var pointText = $(this).text().split(',');
            routeObject.points.push([pointText[0], pointText[1]]);
          });
        }
        routeListArray.push(routeObject);
        i++;
      }
    }

    function getRoutesHTML(applicationScreen)
    {
      var i, l, html = [];

      for (i=0,l=routeListArray.length;i<l;i++)
      {
        if (applicationScreen === "routes") {
          return screenRoutes();
        } else if (applicationScreen === "after-video") {
          return screenAfterVideo();
        } else {
          return;
        }
      }

      function screenRoutes()
      {
        var topBottom, item, f, k, friends;

        for (i=0,l=routeListArray.length;i<l;i++) {
          topBottom = i % 2 ? 'bottom' : 'top';
          item = Math.floor((i+2)/2);
          html.push(replaceDataInRouteBox(routeListArray[i], {topBottom: topBottom, item: item}));
        }

        function replaceDataInRouteBox(routeObject, options) {
          var str = Templates.routeBox, $str;

          str = str.replace('__class__', options.topBottom)
            .replace('__dataItem__', options.item)
            .replace('__routeId__', i)
            .replace('__listImg__', '/assets/img/routes/' + routeObject.image)
            .replace(/__routeName__/g, SkodaBackendConfig.i18n["route_"+(routeObject.id+1)]);

          return str;
        }

        return html;
      }

      function screenAfterVideo()
      {

        for (i=0,l=routeListArray.length;i<l;i++) {
          html.push(replaceDataInRouteBox(routeListArray[i]));
        }

        function replaceDataInRouteBox(routeObject, options) {
          var str = Templates.afterRideRouteBox;

          str = str.replace('__dataItem__', i)
            .replace('__routeId__', i)
            .replace('__listImg__', '/assets/img/routes/' + routeObject.image)
            .replace(/__routeName__/g, SkodaBackendConfig.i18n["route_"+(parseInt(routeObject.id,10)+1)]);

          return str;
        }

        return html;
      }
    }

    function registerEvents()
    {
      // External links
      $("body").on("click", ".external-link", function(e)
      {
        var t = $(this),
          p = t.parents('.page');
        if (p.length !== 0) {
          ga('send', "event", "Button", p.data('page'), t.data("info"));
        } else {
          ga('send', "event", "Button", "Footer", t.data("info"));
        }
      });

      // Maps error
      $("body").on("click", ".maps-error", function(e) {
        $(this).hide();
        e.preventDefault();
      });

      // General body onclick handler for various situations
      $("body").on("click", function(e) {
        var moreList = $('.more-list'),
            target = $(e.target);

        if (moreList.hasClass('act') && !target.hasClass('more-list') && target.parents('.more-list').length === 0) {
          moreList.removeClass('act');
          e.preventDefault();
        }
      });
    }

    function changeTestDrive() {
      var w = r4.cache.window.width(),
        h = r4.cache.window.height(),
        list = $('.list .ul-list > li');

      r4.utils.detaillistclose(w, h, list);
      Skoda.Router.navigate(2);
      r4.utils.header('on');
    }

    return {
      getRoutesHTML: getRoutesHTML,
      routeList: routeListArray,
      changeTestDrive: changeTestDrive,
      registerEvents: registerEvents
    };

  })();

  // --------------------------------------------------------------------------
  // Router
  // --------------------------------------------------------------------------

  var Router = (function() {

    var Routing = {
      pages: {},
      controller: {},
      events: {
        init: {},
        exit: {}
      },
      step: null,
      currentPage: $('.page.page-active').data('pid')
    };

    r4.cache.page.each(processPage);

    function processPage()
    {
      var page = $(this);
      Routing.pages[page.data('pid')] = page;
    }

    $('body').on('click', '.page-step', bindPageActions);

    function bindPageActions(evt)
    {
      var p = $(this),
          oldPageId, newPage, newPageId;

      if (p.parents('.page').length) {
        oldPageId = p.parents('.page').data('pid');
      } else {
        oldPageId = Routing.currentPage;
      }

      newPage = Routing.pages[p.data('step')];
      if (newPage === undefined) {
        return false;
      }
      newPageId = newPage.data('pid');
      Routing.step = p;

      if (p.data('controller') && Routing.controller[p.data('controller')])
      {
        Routing.controller[p.data('controller')](function() {
          switchToPage(Routing.pages[oldPageId], newPage);
        }, {page: p});
      }
      else
      {
        switchToPage(Routing.pages[oldPageId], newPage);
      }
      Routing.step = null;
      evt.preventDefault();
    }

    function switchToPage(oldPage, newPage)
    {
      r4.utils.peekaboo(false, function()
      {
        if (oldPage)
        {
          if (Routing.events.exit[oldPage.data('pid')]) {
            Routing.events.exit[oldPage.data('pid')](function() {
              oldPage.removeClass('page-active');
            });
          } else {
            oldPage.removeClass('page-active');
          }
        }

        if (Routing.events.init[newPage.data('pid')]) {
          Routing.events.init[newPage.data('pid')](function() {
            newPage.addClass('page-active');
          });
        } else {
          newPage.addClass('page-active');
        }

        Routing.currentPage = newPage.data('pid');
        $('.language').removeClass('act');
        r4.utils.calljsforpage();

        if (Routing.currentPage) {
          ga('send', 'pageview', $("page-" + Routing.currentPage).data('page'));
        }
      });
    }

    function navigate(pageId, controller, callback)
    {
      if (Routing.pages[pageId] === undefined) {
        return false;
      }
      if (controller !== undefined && Routing.controller[controller]) {
        Routing.controller[controller](function() {
          switchToPage(Routing.pages[Routing.currentPage], Routing.pages[pageId]);
        });
      } else {
        switchToPage(Routing.pages[Routing.currentPage], Routing.pages[pageId]);
      }
    }

    // --------------------------------------------------------------------------
    // Controllers
    // --------------------------------------------------------------------------

      Routing.controller.GetGoingController = function(callback, options)
      {
        var id = (options.page).parents('li:eq(0)').data('route-id');

        if (DOMHelper.routeList[id].GPS) {
            SkodaMap.getDirections({
              from: [DOMHelper.routeList[id].GPS.fromLat, DOMHelper.routeList[id].GPS.fromLng],
              to: [DOMHelper.routeList[id].GPS.toLat, DOMHelper.routeList[id].GPS.toLng],
              target: [0, 0],
              pitch: 0,
              effectID: 0,
              speed: 7,
              sound: true
          });
        } else {
          SkodaStreetView.preload(DOMHelper.routeList[id].points, [0,0], 7, 0, 0);
        }
        return callback();
      };

    // --------------------------------------------------------------------------
    // Events
    // --------------------------------------------------------------------------

    Routing.events.init[1] = function(callback){

      var a = document.createElement('audio');
      if((!!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''))) || (!!(a.canPlayType && a.canPlayType('audio/wav; codecs="1"').replace(/no/, '')))) {
        var video = document.getElementById('video-player');

        if(video) {
          $('.skoda-play').on('click', function(e){
              $('#video-player-wrap').addClass('is-played');

              r4.utils.header('off');

              video.play();

              e.preventDefault();
          });

          $('#video-player-wrap-close').on('click', function(e){
              $('#video-player-wrap').removeClass('is-played');

              r4.utils.header('on');

              video.pause();

              e.preventDefault();
          });

          video.addEventListener('ended', videoEnded, false);
          function videoEnded(e) {
              if(!e) { e = window.event; }
              $('#video-player-wrap').removeClass('is-played');
          }

          video.addEventListener('webkitendfullscreen', videoExitFullscreen, false);
          function videoExitFullscreen(e) {
              if(!e) { e = window.event; }
              $('#video-player-wrap').removeClass('is-played');
          }
        }
      } else {
        $('.skoda-play').addClass('no-video');
      }

      r4.cache.header.addClass('act');
      setTimeout(function(){
        r4.cache.header.removeClass('act');
      }, 5000);
      return callback();
    };

    Routing.events.exit[1] = function(callback) {
      r4.cache.header.removeClass('act');
      return callback();
    };

    Routing.events.init[2] = function(callback) {
      // Bullshit code needed to keep sly working correctly, gj!
      if (r4.cache.sly) {
        DOMHelper.changeTestDrive();
      }

      // List animate
      $('.ul-list').addClass('animate');
      return callback();
    };

    Routing.events.exit[2] = function(callback) {
      // List animate
      $('.ul-list').removeClass('animate');
      return callback();
    };

    Routing.events.init[3] = function(callback) {
      r4.utils.header('off');
      return callback();
    };

    Routing.events.exit[3] = function(callback) {
      $('.maps-aside, .maps-help, #map-container').show();
      window.SkodaMap.dispose();
      if (!window.SkodaMap) {
        window.SkodaMap = new SkodaMapModule();
      }
      return callback();
    };

    Routing.events.init[4] = function(callback) {
      $('.page-4 .ul-list').html(Skoda.DOMHelper.getRoutesHTML('after-video'));
      return callback();
    };

    Routing.events.exit[4] = function(callback) {
      $('.page-5 .btn-back').show();
      return callback();
    };

    return {
      navigate: navigate
    };

  })();


  var initialize = (function()
  {
    DOMHelper.registerEvents();
    Router.navigate(1);
  })();

  return {
    DOMHelper: DOMHelper,
    Router: Router
  };

})($, r4, SkodaBackendConfig);
