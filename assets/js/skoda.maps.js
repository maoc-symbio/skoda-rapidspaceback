(function() {

    var Map = function(){

        var _config,
            _map,
            _directionsService,
            _directionsDisplay,
            _streetViewService,
            _startPoint = null,
            _endPoint = null,
            _startMarker = null,
            _endMarker = null,
            _circles = null,
            _streetViewLock = false;

        $.ajax({
            type: 'GET',
            url: '/assets/xml/config-mobile.xml',
            cache: false,
            dataType: 'xml',
            success: init
        });

        function init(xml){

            _config = xml;

            // $("#map-container").show();

            initTutorial();

            google.maps.visualRefresh = true;
            if (!window.SkodaStreetView) {
                window.SkodaStreetView = new SkodaStreetViewModule(_config);
            }

            var options = {
                zoom: 10,
                center: new google.maps.LatLng(window.appConfig.gmaps.lat, window.appConfig.gmaps.lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                scaleControl: false,
                overviewMapControl: false,
                streetViewControl: false,
                panControl: false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                }
            };


            _map = new google.maps.Map(document.getElementById('map'), options);

            _map.setOptions({
                styles: [
                    { "featureType": "water", "stylers": [ {"hue": "#0091ff"}, {"saturation": -100}, {"lightness": -11} ] },
                    { "featureType": "administrative", "stylers": [ {"hue": "#00ffe6"}, {"weight": 0.1}, {"visibility": "on"} ] },
                    { "featureType": "landscape.natural.terrain", "stylers": [ {"visibility": "off"} ] },
                    { "featureType": "road", "stylers": [ {"visibility": "simplified"}, {"saturation": -56}, {"hue": "#08ff00"} ] },
                    { "featureType": "administrative.country", "stylers": [{"visibility": "off"} ] }
                ]
            });


            var input = appConfig.isTablet ? document.getElementById('searchBox') : document.getElementById('search-box');
            var searchBox = new google.maps.places.SearchBox(input);
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                var place = searchBox.getPlaces();
                if (place[0].geometry) {
                var location = place[0].geometry.location;

                if(place[0].geometry.viewport){
                    _map.fitBounds(place[0].geometry.viewport);
                }else{
                    _map.panTo(location);
                    _map.setZoom(16);
                }
                _map.overlayMapTypes.setAt(0, _streetViewLayer);
                input.value = "";
            }
            });

            _directionsService = new google.maps.DirectionsService();
            _directionsDisplay = new google.maps.DirectionsRenderer();
            _directionsDisplay.setMap(_map);
            _streetViewService = new google.maps.StreetViewService();

            google.maps.Circle.prototype.contains = function(latLng) {
              return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
            };
        }

        function initTutorial(){

            var _sb = appConfig.isTablet ? $('#searchBox') : $('#search-box');
            _sb.attr("placeholder", SkodaBackendConfig.i18n['search_box_placeholder'] );

            var select = $('#map-select'), selectAppend = '<option value="-1">' + select.data('choose') + '</option>';
            select.empty();

            $(_config).find('moods mood').each(function(){
                var node = $(this);
                selectAppend += '<option value="' + node.attr("id") + '">' + node.find("name").text() + '</option>';
            });
            select.append(selectAppend);

            $('#map-create-button').off("click");
            $('#map-create-button').on("click", function(e){
                /*$('#map').animate({height: $('.create-content').innerHeight()*//*(r4.cache.window.height() - $('#footer').height() - $('.create-header').height())*//*}, function() {
                    google.maps.event.trigger(map, 'resize');
                });*/

                // EDIT
                $('.create-content').css({
                    height: $(window).innerHeight() - $('#footer').innerHeight(),
                    padding: 0
                });
                $('.create-header').hide();
                $('#map-tutorial').hide();
                $('#map').animate({height: $(window).innerHeight() - $('#footer').height()}, function() {
                    google.maps.event.trigger(_map, 'resize');
                });

                google.maps.event.addListener(_map, 'click', handleMapClick);

                //$('#map-tutorial').hide();
                $('#map-container').show();
                e.preventDefault();
            });

            $('#maps-create-btn').off("click");
            $('#maps-create-btn').on("click", function(e){
                $('.maps-help').hide();
                $('.maps-content').show();
                $('#map-container').css('opacity', 1).show();
                google.maps.event.addListener(_map, 'click', handleMapClick);
                google.maps.event.trigger(_map, 'resize');
                e.preventDefault();
            });
        }

        function handleMapClick(event){
            if(_streetViewLock) return;
            _streetViewLock = true;

            var request = {
                origin: event.latLng,
                destination: event.latLng,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

            _directionsService.route(request, onDirectionsComplete);

            /*************************************
            * INIT STREETVIEW MODULE
            *************************************/
            if(_startPoint){
                //SkodaStreetView.initAudio($('#map-select').val());
            }

        }

        function onDirectionsComplete(data, status){

            if(status != google.maps.DirectionsStatus.OK){
                _streetViewLock = false;
                alert( SkodaBackendConfig.i18n['maps_error_no_roads'] );
                return;
            }

            checkForStreetView(data, onCheckForStreetView);

        }

        function checkForStreetView(data, callback){
            _streetViewService.getPanoramaByLocation(data.routes[0].legs[0].start_location, 500, callback);
        }

        function removeStartPoint(){

            $("#map-clear-button").hide();

            _startMarker.setMap(null);
            _startPoint = null;
            _streetViewLock = false;

            var i, l = _circles.length;
            for (i = 0; i < l; i++){
                _circles[i].setMap(null);
            }
            _circles = null;
        }

        function onCheckForStreetView(data, status){

            if (status != 'OK'){
                _streetViewLock = false;
                alert( SkodaBackendConfig.i18n['maps_error_no_streetview'] );
                return;
            }


            if (!_startPoint) {

                $("#map-clear-button").show();
                $("#map-clear-button").off("click");
                $("#map-clear-button").on("click", removeStartPoint);


                _startPoint = data.location.latLng;
                _startMarker = new google.maps.Marker({
                                position: _startPoint,
                                map: _map
                });

                _circles = [];
                _circles.push( new google.maps.Circle({
                                map: _map,
                                center: new google.maps.LatLng(_startPoint.lat(), _startPoint.lng()),
                                strokeWeight: 0,
                                fillColor: "#35c907",
                                fillOpacity: 0.3,
                                clickable: false,
                                radius: 18362.55489862987
                }));

                _circles.push( new google.maps.Circle({
                                map: _map,
                                center: new google.maps.LatLng(_startPoint.lat(), _startPoint.lng()),
                                strokeColor: "#FFFFFF",
                                strokeOpacity: 1,
                                strokeWeight: 0.4,
                                fillColor: "#179805",
                                fillOpacity: 0.5,
                                clickable: false,
                                radius: 919
                }));

                _circles.push( new google.maps.Circle({
                                map: _map,
                                center: new google.maps.LatLng(_startPoint.lat(), _startPoint.lng()),
                                strokeColor: "#FFFFFF",
                                strokeOpacity: 1,
                                strokeWeight: 1,
                                fillColor: "#179805",
                                fillOpacity: 0,
                                clickable: false,
                                radius: 9160
                }));

                _streetViewLock = false;

                return;
            }


            if (!_endPoint) {


                if(_circles[1].contains(data.location.latLng)){
                    _streetViewLock = false;
                    alert( SkodaBackendConfig.i18n['maps_error_further_destination'] );
                    return;

                }else if(!_circles[0].contains(data.location.latLng)){
                    _streetViewLock = false;
                    alert( SkodaBackendConfig.i18n['maps_error_closer_destination'] );
                    return;
                }

                _endPoint = data.location.latLng;
                _endMarker = new google.maps.Marker({
                                    position: _endPoint,
                                    map: _map
                });

                getDirections({
                    from: [_startPoint.lat(), _startPoint.lng()],
                    to: [_endPoint.lat(), _endPoint.lng()],
                    target: [0, 0], // FIX ME
                    pitch: 0, // FIX ME
                    effectID: 0, // FIX ME
                    speed: 7,
                    sound: true // FIX ME
                });

            }

        }

        function getDirections(params){
            var routeLength = 0,
                request = {
                    origin: new google.maps.LatLng(params.from[0], params.from[1]),
                    destination: new google.maps.LatLng(params.to[0], params.to[1]),
                    travelMode: google.maps.TravelMode.DRIVING
                };

            _directionsService.route(request, function(response, status) {

                if (status != google.maps.DirectionsStatus.OK) {
                    alert( SkodaBackendConfig.i18n['maps_error_bad_points'] );
                    dispose();
                    return;
                }

                parsePoints(response);
                routeLength = response.routes[0].legs[0].distance.value;

                var route = response.routes[0].overview_path;
                for (var i = 0; i < route.length; i++) {
                    route[i] = [route[i].lat(), route[i].lng()];
                }

                SkodaStreetView.preload(route, params.target, params.speed, params.pitch, params.effectID);

                if(!appConfig.isTablet) {
                    dispose();
                }
            });
        }

        function parsePoints(response) {
            var route = response.routes[0];
            var path = route.overview_path;
            var legs = route.legs;
            var raw_points = [];
            var _d = 20;

            var total_distance = 0;
            for (var i = 0; i < legs.length; ++i) {
                total_distance += legs[i].distance.value;
            }

            var segment_length = total_distance / 500;
            _d = (segment_length < 20) ? _d = 20 : _d = segment_length;

            var d = 0;
            var r = 0;
            var a, b;

            for (i = 0; i < path.length; i++) {
                if (i + 1 < path.length) {

                    a = path[i];
                    b = path[i + 1];
                    d = google.maps.geometry.spherical.computeDistanceBetween(a, b);

                    if (r > 0 && r < d) {
                        a = pointOnLine(r / d, a, b);
                        d = google.maps.geometry.spherical.computeDistanceBetween(a, b);
                        raw_points.push(a);

                        r = 0;
                    } else if (r > 0 && r > d) {
                        r -= d;
                    }

                    if (r === 0) {
                        var segs = Math.floor(d / _d);

                        if (segs > 0) {
                            for (var j = 0; j < segs; j++) {
                                var t = j / segs;

                                if (t > 0 || (t + i) === 0) { // not start point
                                    var way = pointOnLine(t, a, b);
                                    raw_points.push(way);
                                }
                            }

                            r = d - (_d * segs);
                        } else {
                            r = _d * (1 - (d / _d));
                        }
                    }

                } else {
                    raw_points.push(path[i]);
                }
            }

            response.routes[0].overview_path = raw_points;
        }

        function pointOnLine(percent, a, b) {
            var lat1 = a.lat();
            var lon1 = a.lng();
            var lat2 = b.lat();
            var lon2 = b.lng();
            return new google.maps.LatLng(lat1 + percent * (lat2 - lat1), lon1 + percent * (lon2 - lon1));
        }

        function dispose(){
                if (_startMarker) {
                    _startMarker.setMap(null);
                }
                if (_endMarker) {
                    _endMarker.setMap(null);
                }
                _startPoint = null;
                _endPoint = null;
                _streetViewLock = false;


                if(_circles){
                    var i, l = _circles.length;
                    for (i = 0; i < l; i++){
                        _circles[i].setMap(null);
                    }
                }

                $("#map-clear-button").hide();
                $("#map-container").hide();

            if(!appConfig.isTablet) {
                $("#map").empty();
            }

            delete window.SkodaMap;
            if (window.SkodaStreetView && appConfig.isTablet) {
                window.SkodaStreetView.dispose();
                delete window.SkodaStreetView;
            }
        }

        return {
            dispose: dispose,
            getDirections: getDirections
        };
    };

    window.SkodaMapModule = Map;

})();