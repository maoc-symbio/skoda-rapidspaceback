/*!
 *
 *  Project:  Skoda Rapid Spaceback
 *  Author:   Petr Urbanek - www.r4ms3s.cz
 *  Twitter:  @r4ms3scz
 *
 * @param {Object} window, document, undefined
 *
 */

 (function(window, document, undefined) {

    // Easing
    // =====================================
    $.extend($.easing,
    {
        def: 'easeOutQuad',
        swing: function (x, t, b, c, d) {
            //alert($.easing.default);
            return $.easing[$.easing.def](x, t, b, c, d);
        },
        easeInQuad: function (x, t, b, c, d) {
            return c*(t/=d)*t + b;
        },
        easeOutQuad: function (x, t, b, c, d) {
            return -c *(t/=d)*(t-2) + b;
        },
        easeInOutQuad: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t + b;
            return -c/2 * ((--t)*(t-2) - 1) + b;
        }
    });


    // Defaults
    // =====================================

    var r4 = window.r4 = {
        utils : {},
        cache : {}
    };

    // Methods
    // =====================================

    r4.utils.init = function() {
        r4.cache.window                = $(window);
        r4.cache.document              = $(document);
        r4.cache.html                  = $('html');
        r4.cache.body                  = $('body');

        r4.cache.page                  = r4.cache.body.find('.page');
        r4.cache.header                = r4.cache.body.find('header');
        r4.cache.footer                = r4.cache.body.find('footer');
        r4.cache.section               = r4.cache.body.find('section');

        r4.cache.audio                 = $('.audio audio')[0];

        r4.cache.sly;
        r4.utils.calljsforpage();

        // MEDIA QUERIES
        r4.mobile = false;
        r4.bounds = [
            [1000, 10000, function() {
                mobile = false;
            }],
            [0, 1000, function() {
                mobile = true;
            }]
        ],
        r4.lastBound = -1;
    };

    r4.utils.header = function(status){
        r4.cache.header.off('mouseenter mouseleave').addClass('hide');

        if(status === 'on'){
            r4.cache.header.removeClass('hide')
            r4.cache.header.on('mouseenter mouseleave', function(e){
                if(e.type === 'mouseenter'){
                    r4.cache.header.addClass('act');
                }else{
                    r4.cache.header.removeClass('act');
                }
            });
        }
    };

    r4.utils.footer = function(){
        var langbtn = r4.cache.footer.find('.btn-language'),
            langcontent = $('.language'),
            policybtn = $('.btn-privacy-policy, .privacy-policy .btn-green'),
            policycontent = $('.privacy-policy'),
            noGravitySoundtrackBtn = $('.btn-no-gravity-soundtrack, .no-gravity-soundtrack .btn-green'),
            noGravitySoundtrackContent = $('.no-gravity-soundtrack'),
            noGravityRingtoneBtn = $('.btn-no-gravity-ringtone, .no-gravity-ringtone .btn-green'),
            noGravityRingtoneContent = $('.no-gravity-ringtone'),
            noGravityRingtoneIphoneBtn = $('.btn-no-gravity-ringtone-iphone, .no-gravity-ringtone-iphone .btn-green'),
            noGravityRingtoneIphoneContent = $('.no-gravity-ringtone-iphone'),
            more = r4.cache.footer.find('.more'), morebtn, morecontent;

        more.each(function(){
            var more = $(this),
            morebtn = more.find('> a'),
            morecontent = more.find('ul');

            morecontent.css({
                bottom: '-' + morecontent.height() + 'px'
            });

            morebtn.on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                more.addClass('more-actual');
                r4.cache.footer.find('.more').not('.more-actual').find('ul').removeClass('act');
                more.removeClass('more-actual');

                morecontent.toggleClass('act');
            });
        });

        $('.after-more-music a').on('click', function(e){
            $('footer .more-music .more-list').toggleClass('act');
            e.stopPropagation();
            e.preventDefault();
        });

        langbtn.on('click', function(e){
            e.preventDefault();

            langcontent.toggleClass('act');
        });

        policybtn.on('click', function(e){
            e.preventDefault();

            policycontent.toggleClass('act');
        });

        noGravitySoundtrackBtn.on('click', function(e){
            e.preventDefault();

            $(this).parent().parent().removeClass('act');
            noGravityRingtoneContent.removeClass('act');
            noGravityRingtoneIphoneContent.removeClass('act');

            noGravitySoundtrackContent.toggleClass('act');
        });

        noGravitySoundtrackContent.find('a').not('.btn-green').on('click', function(e){
            ga('send', "event", "Download", "Music", $(this).text());
        });

        noGravityRingtoneBtn.on('click', function(e){
            e.preventDefault();

            $(this).parent().parent().removeClass('act');
            noGravitySoundtrackContent.removeClass('act');
            noGravityRingtoneIphoneContent.removeClass('act');

            noGravityRingtoneContent.toggleClass('act');
        });

        noGravityRingtoneContent.find('a').not('.btn-green').on('click', function(e){
            ga('send', "event", "Download", "Music", $(this).text());
        });

        noGravityRingtoneIphoneBtn.on('click', function(e){
            e.preventDefault();

            $(this).parent().parent().removeClass('act');
            noGravityRingtoneContent.removeClass('act');
            noGravitySoundtrackContent.removeClass('act');

            noGravityRingtoneIphoneContent.toggleClass('act');
        });

        noGravityRingtoneIphoneContent.find('a').not('.btn-green').on('click', function(e){
            ga('send', "event", "Download", "Music", $(this).text());
        });
    };


    // HOME PRELOAD
    r4.utils.resizeObj = function(w, h){
        //return;

        var elw = 1280,
            elh = 665,
            scale = Math.max(h / elh , w / elw),
            width = Math.ceil(scale * elw),
            height = Math.ceil(scale * elh),
            left = Math.ceil((w - width) / 2),
            top = Math.ceil((h - height) / 2),
            bgImgPosition = 'fixed';

        if(appConfig.isTablet) {
            bgImgPosition = 'absolute';
        }

        $('.bg-img img')
            .css('position', bgImgPosition)
            .css('width', width + 'px')
            .css('height', height + 'px')
            .css('left', left + 'px')
            .css('top', top + 'px');

        var elw = 632,
            elh = 394,
            scale = Math.max(h / elh , w / elw),
            width = Math.ceil(scale * elw),
            height = Math.ceil(scale * elh),
            left = Math.ceil((w - width) / 2),
            top = Math.ceil((h - height) / 2);

        $('#video, #video .vjs-tech')
            .width(width).height(height)
            .css('position', bgImgPosition)
            .css('width', width + 'px')
            .css('height', height + 'px')
            .css('left', left + 'px')
            .css('top', top + 'px');   

        if($('.page-2 .noanim').length) {
            r4.utils.detaillist(w, h, $('.page-2 .noanim'));                     
        }
    };


    // HOME VIDEO
    r4.utils.resizevideo = function(w, h){
        var elw = 632,
            elh = 394,
            scale = Math.max(h / elh , w / elw),
            width = Math.ceil(scale * elw),
            height = Math.ceil(scale * elh),
            left = Math.ceil((w - width) / 2),
            top = Math.ceil((h - height) / 2);

        // ANIMATION - AFTER LOAD
        setTimeout(function(){
            $('.home').addClass('after-load');

            // FIXed!
            // Safari Vs. IE Vs. other
            var isChrome = !!window.chrome && !!window.chrome.webstore,
                isSafari = /Constructor/.test(window.HTMLElement),
                isOpera = !!window.opera,
                ieVersion = (function() { if (new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null) { return parseFloat( RegExp.$1 ); } else { return false; } })(),
                isIE8 = document.all && document.querySelector && !document.addEventListener,
                autoplayparam = false;

            if(isChrome || isIE8){
                autoplayparam = true;
            }

            if(!appConfig.isTablet) {
                videojs("video", { "controls": true, "autoplay": autoplayparam, "preload": "auto" }).ready(function(){

                    r4.cache.homevideo = this;

                    // PAUSE
                    /*
                    if(isChrome) {
                        r4.cache.homevideo.pause();
                    }
                    */

                    // LOAD BUFFER
                    var bufferinterval = window.setInterval(function() {
                        $('.home .buffer strong').css({ width: Math.ceil(r4.cache.homevideo.bufferedPercent() * 100) + '%' });

                        if(Math.ceil(r4.cache.homevideo.bufferedPercent() * 100) >= 90){
                            window.clearInterval(bufferinterval);

                            $('.home .buffer strong').hide();

                            $('.home .bg-img').addClass('done');
                            $('.home section').addClass('load-complete');
                            r4.cache.homevideo.play();
                        }
                    }, 200);

                    r4.cache.homevideo.dimensions(width, height);
                    //r4.cache.homevideo.requestFullScreen(); // BUGGED in IE9

                    // this.volume(0);

                    $('#video')
                        .width(width).height(height)
                        .css('position', 'fixed')
                        .css('width', width + 'px')
                        .css('height', height + 'px')
                        .css('left', left + 'px')
                        .css('top', top + 'px');

                    // AUDIO
                    r4.utils.audio.setObject(r4.cache.homevideo, true);

                });
            }
        }, 500);

    };

    r4.utils.audio = {

        isPlaying: true,
        isFlashPlaying: false,
        isVideo: false,
        obj: null,

        play: function()
        {
            if(!r4.utils.audio.isFlashPlaying) {
                r4.utils.audio.obj.play();
                r4.utils.audio.isPlaying = true;
            }
        },
        pause: function()
        {
            r4.utils.audio.obj.pause();
            r4.utils.audio.isPlaying = false;
        },
        volume: function(vol)
        {
            r4.utils.audio.obj.volume(vol);
        },
        init: function()
        {
            r4.cache.footer.find('.sound-on').on('click', function(e){
                e.preventDefault();
                r4.cache.footer.find('.sound-on').removeClass('act');
                r4.cache.footer.find('.sound-off').addClass('act');

                if (r4.utils.audio.isVideo) {
                    r4.utils.audio.volume(1);
                    r4.utils.audio.isPlaying = true;
                } else {
                    r4.utils.audio.play();
                }

                if (Skoda.Flash.flashDOM) {
                    Skoda.Flash.flashDOM.unmute();
                }
            });
            r4.cache.footer.find('.sound-off').on('click', function(e){
                e.preventDefault();
                r4.cache.footer.find('.sound-on').addClass('act');
                r4.cache.footer.find('.sound-off').removeClass('act');

                if (r4.utils.audio.isVideo) {
                    r4.utils.audio.volume(0);
                    r4.utils.audio.isPlaying = false;
                } else {
                    r4.utils.audio.pause();
                }

                if (Skoda.Flash.flashDOM) {
                    Skoda.Flash.flashDOM.mute();
                }
            });
        },
        setObject: function(obj, isVideo) {
            r4.utils.audio.obj = obj;
            r4.utils.audio.isVideo = isVideo ? true : false;
        }
    };

    // LIST
    r4.utils.list = function(w, h, firstload){

        var listtop = $('.list .list-slider li.top'),
            listbottom = $('.list .list-slider li.bottom'),
            postop = 50,
            posbottom = 0;

        // LIST TOP ARTICLE
        listtop.each(function(){
            var el = $(this),
                pos = el.data('item'),
                newpos = ((pos * 415) - 365);

            el.css({
                left: newpos + 'px'
            }).data('left', newpos);

            postop = newpos;
        });

        // LIST BOTTOM ARTICLE
        listbottom.each(function(){
            var el = $(this),
                pos = el.data('item'),
                newpos = ((pos * 415) - 165);

            el.css({
                left: newpos + 'px'
            }).data('left', newpos);

            posbottom = newpos;
        });

        // COMPLETE SLIDER SIZE
        var slidersize = Math.max(postop , posbottom);
        $('.list .list-slider .slider').css({
            width: slidersize + 415 + 'px'
        });

        if(firstload === 'true'){
            r4.utils.sly($('.list .slider-content'));
        }

        // DETAIL
        r4.utils.detail(w, h);
    };


    // DETAIL JOURNEY
    r4.utils.detail = function(w, h){
        var actcenter = 0;

        r4.cache.sly.on('move', function() {
            actcenter = r4.cache.sly.pos.cur;
        });

        $('.list .ul-list .list-small').off('click');
        $('.list .ul-list .list-small').on('click', function(e){
            e.preventDefault();

            var a = $(this).parent(),
                li = a.parents('li:eq(0)'),
                leftpos = li.data('left'),
                leftdif = actcenter - leftpos;

            // ANIMATE ACT
            // FF - ANIMATE BUG
            var isFF = !!navigator.userAgent.match(/firefox/i),
                isIE = !+'\v1';
            if(isFF || isIE){
                a.addClass('act').animate({
                    bottom: 'auto',
                    marginTop: 0 + 'px',
                    marginLeft: (leftdif + (w / 2) - 385) + 'px',
                    width: '770px',
                    height: '313px' //315px
                }, 300);
                li.addClass('act');
            }else{
                a.addClass('act').css({
                    bottom: 'auto',
                    marginTop: 0 + 'px',
                    marginLeft: (leftdif + (w / 2) - 385) + 'px',
                    width: '770px',
                    height: '313px' //315px
                });
                li.addClass('act');
            }

            $('.list .create-own').addClass('hide-detail');

            // DISABLE OTHER
            $('.list .ul-list article[class!="act"]').addClass('done');

            setTimeout(function(){
                $('.list .ul-list article').off('click');
                a.addClass('noanim');
                r4.utils.detaillist(w, h, a);
            }, 500);

        });
    };


    // LIST - DETAIL
    r4.utils.detaillist = function(w, h, a){
        var list = $('.list .ul-list > li'),
            slideto = 0,
            slidersize = 0,
            pos = 0,
            slideto = 0;

        list.each(function(){
            var el = $(this);

            el.css({
                left: 'auto',
                position: 'relative',
                width: w + 'px'
            }).data('left', pos);

            if(el.hasClass('act')){
                slideto = pos;
            }

            pos = pos + w;
        });

        $('.list .list-slider .slider').css({
            width: pos + 'px'
        });
        $('.list .list-slider .slider-content').addClass('show-detail');
        $('.list .list-info').addClass('show-detail');
        $('.list .create-own').addClass('show-detail').removeClass('hide-detail');

        r4.cache.sly.destroy();
        r4.cache.sly.set({ mouseDragging: 0, scrollBy: 0 });
        r4.cache.sly.init();

        a.attr('style', '').removeAttr('style').css({
            position: 'fixed',
            left: '50%',
            top: '50%',
            marginLeft: '-385px',
            marginTop: '-157px', //-160px
            width: '770px',
            height: '313px' //315px
        });

        r4.cache.sly.reload();
        r4.cache.sly.slideTo(slideto, true);
        a.css({
            position: 'absolute',
            marginTop: '-157px'
        });

        r4.utils.detailroute(pos, slideto, w);

        $('.list .close').off('click');
        setTimeout(function(){
            $('.close').on('click', function(e){
                e.preventDefault();

                $('.list .list-slider').addClass('close-detail');

                setTimeout(function(){
                    r4.utils.detaillistclose(w, h, list);
                }, 300);
            });
        }, 200);
    };


    // LIST - DETAIL ROUTE
    r4.utils.detailroute = function(pos, slideto, w){
        var countpage = pos / w,
            scrollto = slideto,
            actual = (scrollto / w) + 1;

        r4.cache.sly.off('moveEnd');
        r4.cache.sly.on('moveEnd', function(){
            var scrollto = r4.cache.sly.pos.dest,
                actual = (scrollto / w) + 1;

            $('.list .list-info h2 span').html(actual + '/' + countpage);
        });

        $('.list .list-info h2 span').html(actual + '/' + countpage);
    };


    // LIST - DETAIL CLOSE
    r4.utils.detaillistclose = function(w, h, list){
        r4.cache.sly.destroy();
        r4.cache.sly.set({ mouseDragging: 1, scrollBy: 100 });

        $('.list .list-slider .slider-content').removeClass('show-detail');
        $('.list .list-info').removeClass('show-detail');
        $('.list .create-own').removeClass('show-detail');

        list.each(function(){
            var el = $(this);

            el.attr('style', '').removeAttr('style').removeClass('act');
            el.find('article').attr('style', '').removeAttr('style').removeAttr('class');
        });

        r4.utils.list(w, h, 'false');

        r4.cache.sly.reload();
        r4.cache.sly.init();

        setTimeout(function(){
            $('.list .list-slider').removeClass('close-detail');
        }, 300);
    };


    // SLY
    r4.utils.sly = function(layout){
        var wrap = layout.parent(),
            options = {
                horizontal: 1,
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                scrollBy: 100,
                activatePageOn: 1,
                speed: 300,
                elasticBounds: 0, //1
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,

                prevPage: wrap.parent().parent().find('.list-info .btn-prev'),
                nextPage: wrap.parent().parent().find('.list-info .btn-next')
            },
            sly = new Sly(layout, options);

        r4.cache.sly = sly;
    };

    // MAPS
    r4.utils.maps = function(w, h){
        var elw = 1280,
            elh = 665,
            scale = Math.max(h / elh , w / elw),
            width = Math.ceil(scale * elw),
            height = Math.ceil(scale * elh),
            left = Math.ceil((w - width) / 2),
            top = Math.ceil((h - height) / 2);

        $('.maps img.help')
            .css('position', 'fixed')
            .css('width', width + 'px')
            .css('height', height + 'px')
            .css('left', left + 'px')
            .css('top', top + 'px');

        if(!appConfig.isTablet) {
            $('#map-container').css('opacity', 0);
            $('#maps-create-btn').on('click', function(e) {
                $('#map-container').animate({
                    opacity: 1
                }, 350, function(){
                    Skoda.Map.setCustomRouteBeingCreated(true);
                    $('.maps-help').hide();
                    r4.utils.header('off');
                });

                e.preventDefault();
            });
        }
    };


    // AFTER VIDEO
    r4.utils.aftervideo = function(w, h){
        var elw = 1280,
            elh = 665,
            scale = Math.max(h / elh , w / elw),
            width = scale * elw,
            height = scale * elh,
            left = (w - width) / 2,
            top = (h - height) / 2;

        var list = $('.after-video .list-slider li'),
            sizepos = 0,
            slideto = 0;

        // LIST ARTICLE
        list.each(function(){
            var el = $(this),
                pos = el.data('item');

            el.attr('style', '').removeAttr('style').css({
                left: sizepos + 'px'
            }).data('left', sizepos);

            // SLIDE TO
            if(el.find('article').hasClass('selected-ride')){
                slideto = sizepos;
            }

            sizepos = sizepos + 365;
        });

        // COMPLETE SLIDER SIZE
        $('.after-video .list-slider .slider').css({
            width: sizepos + 'px'
        });

        // EDIT SLY
        //r4.cache.sly.destroy();
        r4.utils.sly($('.after-video .slider-content'));
        r4.cache.sly.set({
            mouseDragging: 0,
            scrollBy: 0
        });
        r4.cache.sly.init();
        r4.cache.sly.slideTo(slideto, true);
    };


    // STATISTICS
    r4.utils.statistics = function(w, h){

    };


    // RESIZE
    r4.utils.resize = function(start){
        r4.utils.calcBounds(r4.cache.window, r4.bounds, r4.lastBound, start);
    };


    // RESIZE METHODS
    r4.utils.calcBounds = function(ths, bounds, lastBound, start) {
        var w = ths.width(),
            h = ths.height();

        for(var i = 0, j = bounds.length; i < j; i++) {
            if(w > bounds[i][0] && w < bounds[i][1] && lastBound !== i) {
                bounds[i][2]();
                lastBound = i;
            }
        }

        r4.utils.resizeObj(w, h);
    };


    r4.utils.peekaboo = function(goBack, callback){
        var peekaboo = $('#peekaboo');

        if(goBack) {
            /*peekaboo.css('right', '-10%').stop().animate({
                'right' : '100%'
            }, 250, 'easeInQuad');*/

            peekaboo.css('right', '-10%').stop().fadeTo(350, 0, function(){
                peekaboo.css({
                    'right' : '100%'
                }).fadeTo(10, 1);
            });
        } else {
            peekaboo.stop().animate({
                'right' : '-10%'
            }, 250, 'easeOutQuad', function(){
                if (typeof callback == 'function') {
                    callback();
                }
                /*peekaboo.stop().animate({
                    'right' : '100%'
                }, 250, 'easeInQuad');*/
                peekaboo.stop().fadeTo(350, 0, function(){
                    peekaboo.css({
                        'right' : '100%'
                    }).fadeTo(10, 1);
                });
            });
        }
    };

    r4.utils.calljsforpage = function(){
        var w = r4.cache.window.width(),
            h = r4.cache.window.height();

        if($('.page-active.home').length){

            // STOP PLAY VIDEO
            r4.utils.resizevideo(w, h);
            //$('.home').addClass('after-load');
        }
        if($('.page-active.list').length){
            r4.utils.list(w, h, 'true');
            r4.cache.sly.init();
        }
        if($('.page-active.maps').length){
            r4.utils.maps(w, h);
        }
        if($('.page-active.after-video').length){
            r4.utils.aftervideo(w, h);
        }
        if($('.page-active.statistics').length){
            r4.utils.statistics(w, h);
        }
    };

    r4.utils.domLoad = function() {

        r4.utils.resize();
        r4.cache.window.smartresize(function() {
            r4.utils.resize();
        });

        // HEADER ANIMATE ON
        r4.utils.header('on');
        r4.utils.footer();

        // Turn off audio
        var a = document.createElement('audio');
        if((!!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''))) || (!!(a.canPlayType && a.canPlayType('audio/wav; codecs="1"').replace(/no/, '')))) {
            r4.utils.audio.init();
        }

        // SELECT 2
        $(".maps-aside select").select2({
            placeholder: "Choose"
        });

        $('.maps-error .close').on('click', function(e) {
            $(this).parents('.maps-error').hide();
            e.preventDefault();
        });

        // ANIMATE CAR
        var animatecartime = setTimeout(function(){ animatecar(); }, 2000);
        var animatecar = function(){
            clearTimeout(animatecartime);

            var act = $('.after-video .car img.act'),
                next = act.next();

            if(!(next.length)){
                next = $('.after-video .car img:first-child');
            }

            act.fadeTo(300, 0, function(){
                act.removeClass('act');
            });
            next.delay(150).fadeTo(300, 1, function(){
                next.addClass('act');

                animatecartime = setTimeout(function(){ animatecar(); }, 2000);
            });

        };

    };


    // Initialize Events
    // =====================================

    r4.utils.init();

    jQuery(function($) {
        r4.utils.domLoad();
    });


})(window, document);

// debouncing function from John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
(function($,sr) {

    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap) {
                    func.apply(obj, args);
                }
                timeout = null;
            }

            if (timeout) {
                clearTimeout(timeout);
            } else if (execAsap) {
                func.apply(obj, args);
            }
            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    jQuery.fn[sr] = function(fn){ return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
