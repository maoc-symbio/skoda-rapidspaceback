// --------------------------------------------------------------------------
// Facrbook SDK && Google Maps API V3 Initialization
// --------------------------------------------------------------------------

window.fbAsyncInit = function()
{
	FB.init({
		appId: appConfig.appId,
		channelUrl: '//'+ appConfig.host +'/channel.html',
		status: true,
		cookie: true,
		xfbml: false
	});
};

(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

// --------------------------------------------------------------------------
// Skoda application
// --------------------------------------------------------------------------

var Skoda = (function($, r4, SkodaBackendConfig)
{
	// --------------------------------------------------------------------------
	// Templates
	// --------------------------------------------------------------------------

	var Templates = {};
	for (var key in SkodaBackendConfig.Templates) {
		Templates[key] = SkodaBackendConfig.Templates[key];
	}

	// --------------------------------------------------------------------------
	// Facebook helper
	/// --------------------------------------------------------------------------

	var FBH = (function() {
		var User = { name: null, id: null, gender: null, friends: [], lastRide: {id:null, hash:null} };

		function tryFacebookConnect(callback)
		{
			var login_options = { scope: 'email, user_likes, user_friends, publish_stream, publish_actions' };
			FB.getLoginStatus(function(response) {

				if (response.status === "connected") {
					callBackendConnect(response);
				} else {
					FB.login(function(loginResponse) {
						if (loginResponse.status === "connected") {
							callBackendConnect(loginResponse);
						} else {
							if (callback !== undefined && typeof callback === "function") {
								callback();
							}
						}
					}, login_options);
				}

				function callBackendConnect(response) {
					FB.api(response.authResponse.userID, function(userResponse) {
						$.post('/facebook-connect', userResponse, function(connectResponse) {
							FBH.User = connectResponse;
							createFriendSelector();
							if (callback !== undefined && typeof callback === "function") {
								callback();
							}
						}, "JSON");
					});
				}
			});
		}

		/**
		 * Opens facebook UI dialogue and let's you share something
		 * @param post = { name, caption, description, picture }
		 */
		function publishToStream(post)
		{
			FB.ui({
				method: 'feed',
				link: document.URL,
				picture: post.picture,
				name: post.name,
				caption: post.caption,
				description: post.description
			}, function(response) {});
		}

		/**
		 * Saves the ride in to database for later user
		 * User name and id are took from the facebook connect or sent null
		 * @param post = { startPoint, endPoint, targetPoint }
		 */
		function saveRide(data, culture, callback)
		{
			data.name = FBH.User.name;
			data.id = FBH.User.id;
			data.gender = FBH.User.gender;

			$.post('/save/ride', { ride: data, culture: culture }, function(response) {
				callback(response);
			});
		}

		function createFriendSelector()
		{
			TDFriendSelector.init();

			var FriendSelector = TDFriendSelector.newInstance({
				callbackFriendSelected : callbackFriendSelected,
				callbackFriendUnselected : callbackFriendUnselected,
				maxSelection: 4,
				friendsPerPage: 6,
				autoDeselection: true
			});

			FriendSelector.loadPredefinedFriends(FBH.User.friends);

			function callbackFriendSelected(friendId) {

				var friend = {
					name: $('.TDFriendSelector_friendsContainer').find('a[data-id="'+friendId+'"] img').attr('alt'),
					id: friendId
				};
				FBH.User.friends.push(friend);

				$('.friend-list').append(Templates.friendIcon.replace(/__fid__/g, friendId));
			}

			function callbackFriendUnselected(friendId) {
				var i,l;

				$('.friend-list').find('a[data-fid="'+friendId+'"]').parent('li').remove();

				for (i=0,l=FBH.User.friends.length;i<l;i++) {
					if (FBH.User.friends[i].id === friendId) {
						FBH.User.friends.splice(i, 1);
						return;
					}
				}
			}

			$('.page').on('click', '.btn-friend', function(evt)
			{
				FriendSelector.showFriendSelector();
				evt.preventDefault();
			});

			$('.page').on('click', '.friend-list a', function(evt)
			{
				var fid = $(this).data('fid').toString();
				FriendSelector.removeFriend(fid);
				callbackFriendUnselected(fid);
				evt.preventDefault();
			});
		}

		function getFriendsArray()
		{
			var i, l, friendsArray = [];
			for (i=0,l=FBH.User.friends.length;i<l;i++) {
				friendsArray.push('https://graph.facebook.com/'+FBH.User.friends[i].id+'/picture?type=square');
			}
			return friendsArray;
		}

		return {
			User: User,
			tryFacebookConnect: tryFacebookConnect,
			publishToStream: publishToStream,
			saveRide: saveRide,
			getFriendsArray: getFriendsArray
		};

	})();

	// --------------------------------------------------------------------------
	// DOM helper
	// --------------------------------------------------------------------------

	var DOMHelper = (function() {

		var routeListArray = [];

		$.ajax({
			type: 'GET',
			url: '/assets/xml/config.xml',
			cache: false,
			dataType: 'xml',
			success: prepareRouteList
		});

		function prepareRouteList(xml)
		{
			var defaults = { speed: 10, pitch: 0 },
				routeOptions = ['name', 'speed', 'pitch', 'image', 'effect', 'gps'],
				i = 0,
				j = 0,
				l;

			$(xml).find('routes route').each(prepareRoutes);

			$('.page-2 .ul-list').html(DOMHelper.getRoutesHTML('routes'));

			function prepareRoutes()
			{
				var route = $(this), topBottom, _r = {};
				for (j=0,l=routeOptions.length;j<l;j++) {
					_r[routeOptions[j]] = route.find(routeOptions[j]).text();
				}
				_r.points = route.find('points');

				var routeObject = {
					id: i,
					name: _r.name,
					speed: _r.speed.length ? parseFloat(_r.speed) : defaults.speed,
					pitch: _r.pitch.length ? parseFloat(_r.pitch) : defaults.pitch,
					image: _r.image,
					effect: parseFloat(_r.effect),
					GPS: _r.gps.length ? true : false,
					points: _r.points.text().length ? true : false
				};

				if (routeObject.GPS)
				{
					routeObject.GPS = {
						fromLat : parseFloat(route.find('gps from lat').text()),
						fromLng : parseFloat(route.find('gps from lng').text()),
						toLat : parseFloat(route.find('gps to lat').text()),
						toLng : parseFloat(route.find('gps to lng').text()),
						target : route.find('gps from target').length ? parseFloat(route.find('gps from target').text()) : [0, 0]
					};
				}

				if (routeObject.points)
				{
					routeObject.points = [];
					_r.points.find('point').each(function() {
						var pointText = $(this).text().split(',');
						routeObject.points.push([pointText[0], pointText[1]]);
					});
				}
				routeListArray.push(routeObject);
				i++;
			}
		}

		function getRoutesHTML(applicationScreen)
		{
			var i, l, html = [];

			for (i=0,l=routeListArray.length;i<l;i++)
			{
				if (applicationScreen === "routes") {
					return screenRoutes();
				} else if (applicationScreen === "after-video") {
					return screenAfterVideo();
				} else {
					return;
				}
			}

			function screenRoutes()
			{
				var topBottom, item, f, k, friends;

				for (i=0,l=routeListArray.length;i<l;i++) {
					topBottom = i % 2 ? 'bottom' : 'top';
					item = Math.floor((i+2)/2);
					html.push(replaceDataInRouteBox(routeListArray[i], {topBottom: topBottom, item: item}));
				}

				function replaceDataInRouteBox(routeObject, options) {
					var str = Templates.routeBox, $str;

					str = str.replace('__class__', options.topBottom)
						.replace('__dataItem__', options.item)
						.replace('__routeId__', i)
						.replace('__listImg__', '/assets/img/routes/' + routeObject.image)
						.replace(/__routeName__/g, SkodaBackendConfig.i18n["route_"+(routeObject.id+1)]);

					$str = $(str);

					if (FBH.User.id === null)
					{
						$str.find('.detail-friend').remove();
					}
					else
					{
						var friends = getUserFriendsHTML();
						$str.find('.friend-list').html(friends);
					}

					return $str[0].outerHTML;
				}

				return html;
			}

			function screenAfterVideo()
			{
				if ((FBH.User.lastRide.id === null && FBH.User.lastRide.hash !== null) || appConfig.ride !== undefined)
				{
					var hash;
					if (appConfig.ride !== undefined && FBH.User.lastRide.hash === null) {
						hash = appConfig.ride.hash;
					} else {
						hash = FBH.User.lastRide.hash;
					}

					var str = Templates.afterRideRouteBox;
						str = str.replace('__dataItem__', '')
						.replace('__routeId__', '')
						.replace('__listImg__', '/assets/img/routes/user-route.jpg')
						.replace('__class__', 'selected-ride')
						.replace(/__routeName__/g, '');

					var $str = $(str);
					$str.find('img').before(Templates.routeBoxShare.replace(/__routeUrl__/g, 'http://' + appConfig.host + '/' + appConfig.culture + '/ride/' + hash));
					$str.find('.page-step').removeClass('page-step');
					$str.find('a').attr('href', 'http://' + appConfig.host + '/' + appConfig.culture + '/ride/' + hash);

					html.push($str[0].outerHTML);
				}

				for (i=0,l=routeListArray.length;i<l;i++) {
					html.push(replaceDataInRouteBox(routeListArray[i]));
				}

				function replaceDataInRouteBox(routeObject, options) {
					var str = Templates.afterRideRouteBox;

					str = str.replace('__dataItem__', i)
						.replace('__routeId__', i)
						.replace('__listImg__', '/assets/img/routes/' + routeObject.image)
						.replace(/__routeName__/g, SkodaBackendConfig.i18n["route_"+(parseInt(routeObject.id,10)+1)]);

					if(FBH.User.lastRide.id === routeObject.id){
						str = str.replace('__class__', 'selected-ride');
						var $str = $(str);
						$str.find('img').before(Templates.routeBoxShare.replace(/__routeUrl__/g, 'http://' + appConfig.host + '/' + appConfig.culture + '/ride/' + FBH.User.lastRide.hash));
						return $str[0].outerHTML;
					}

					return str;
				}

				return html;
			}
		}

		function registerEvents()
		{
			// External links
			$("body").on("click", ".external-link", function(e)
			{
				var t = $(this),
					p = t.parents('.page'),
					info = t.data('info');
				if (p.length !== 0) {
					ga('send', "event", "Button", p.data('page'), t.data("info"));
				} else {
					ga('send', "event", "Button", "Footer", t.data("info"));
				}

				if (info === "TestDrive") {
					ga('send', "pageview", "/" + appConfig.culture + "/test-drive");
				} else if (info === "SpacebackInfo") {
					ga('send', "pageview", "/" + appConfig.culture + "/model-page");
				}

			});

			// Shares
			$("body").on("click", ".share-ride a", function(e) {
				var t = $(this);
				ga('send', "event", "Share", t.attr('class'));
			});

			// Maps error
			$("body").on("click", ".maps-error", function(e) {
				$(this).hide();
				e.preventDefault();
			});

			// General body onclick handler for various situations
			$("body").on("click", function(e) {
				var moreList = $('.more-list'),
						target = $(e.target);
						
				if (moreList.hasClass('act') && !target.hasClass('more-list') && target.parents('.more-list').length === 0) {
					moreList.removeClass('act');
					e.preventDefault();
				}
			});
		}

		function getUserFriendsHTML()
		{
			var f, _f, k, html = [];
			if (FBH.User.friends !== null) {
				for (f=0,k=FBH.User.friends.length;f<k;f++) {
					_f = FBH.User.friends[f];
					html.push(Templates.friendIcon.replace(/__fid__/g, _f.id));
				}
			}
			return html.join('');
		}

		function hideMap()
		{
			$('.maps-help, .maps-aside, #map-container').hide();
			$('#flashcontainer').show();
		}

		function showMap()
		{
			$('.maps-help, .maps-aside, #map-container').show();
			$('#flashcontainer').hide();
		}

		function embedFlash(callback)
		{
			if ($('#flashcontainer').length === 0) {
				$('.page-3 .maps-content').append($('<div></div>').attr('id', 'flashcontainer'));
			}
			if (callback === undefined || typeof callback !== 'function') {
				callback = function(e){};
			}
			swfobject.embedSWF("/assets/swf/Main.swf", "flashcontainer", "100%", "100%", "10", "/assets/swf/playerProductInstall.swf", {
				"config": "/assets/xml/config.xml"
			}, {
				"wmode": "direct"
			}, null, callback);
		}

		function showMapsError(text)
		{
			var el = $('.maps-error');
			el.find('h1').text(SkodaBackendConfig.i18n[text]);
			el.show();
		}

		function removeFlash(callback)
		{
			swfobject.removeSWF('flashcontainer');
			if (callback !== undefined && typeof callback === 'function') {
				callback();
			}
		}

		function changeTestDrive() {
			var w = r4.cache.window.width(),
				h = r4.cache.window.height(),
				list = $('.list .ul-list > li');

			r4.utils.detaillistclose(w, h, list);
			Skoda.Router.navigate(2);
			r4.utils.header('on');
		}

		function showLoader(){
			if(!$('.full-loader').length) {
				r4.cache.body.append('<div class="full-loader"></div>');
			}
		}

		function hideLoader(){
			$('.full-loader').remove();
		}

		return {
			getRoutesHTML: getRoutesHTML,
			getUserFriendsHTML: getUserFriendsHTML,
			routeList: routeListArray,
			hideMap: hideMap,
			showMap: showMap,
			embedFlash: embedFlash,
			removeFlash: removeFlash,
			changeTestDrive: changeTestDrive,
			showMapsError: showMapsError,
			registerEvents: registerEvents,
			showLoader: showLoader,
			hideLoader: hideLoader
		};

	})();

	// --------------------------------------------------------------------------
	// Router
	// --------------------------------------------------------------------------

	var Router = (function() {

		var Routing = {
			pages: {},
			controller: {},
			events: {
				init: {},
				exit: {}
			},
			step: null,
			currentPage: $('.page.page-active').data('pid')
		};

		r4.cache.page.each(processPage);

		function processPage()
		{
			var page = $(this);
			Routing.pages[page.data('pid')] = page;
		}

		$('body').on('click', '.page-step', bindPageActions);

		function bindPageActions(evt)
		{
			var p = $(this),
					oldPageId, newPage, newPageId;

			if (p.parents('.page').length) {
				oldPageId = p.parents('.page').data('pid');
			} else {
				oldPageId = Routing.currentPage;
			}

			newPage = Routing.pages[p.data('step')];
			if (newPage === undefined) {
				return false;
			}
			newPageId = newPage.data('pid');
			Routing.step = p;

			if (p.data('controller') && Routing.controller[p.data('controller')])
			{
				Routing.controller[p.data('controller')](function() {
					switchToPage(Routing.pages[oldPageId], newPage);
				}, {page: p});
			}
			else
			{
				switchToPage(Routing.pages[oldPageId], newPage);
			}
			Routing.step = null;
			evt.preventDefault();
		}

		function switchToPage(oldPage, newPage)
		{
			r4.utils.peekaboo(false, function()
			{
				if (oldPage)
				{
					if (Routing.events.exit[oldPage.data('pid')]) {
						Routing.events.exit[oldPage.data('pid')](function() {
							oldPage.removeClass('page-active');
						});
					} else {
						oldPage.removeClass('page-active');
					}
				}

				if (Routing.events.init[newPage.data('pid')]) {
					Routing.events.init[newPage.data('pid')](function() {
						newPage.addClass('page-active');
					});
				} else {
					newPage.addClass('page-active');
				}

				Routing.currentPage = newPage.data('pid');
				$('.language').removeClass('act');
				$('.privacy-policy').removeClass('act');
				r4.utils.calljsforpage();

				if (Routing.currentPage) {
					ga('send', 'pageview', '/' + appConfig.culture + '/' + $(".page-" + Routing.currentPage).data('page'));
				}
			});
		}

		function navigate(pageId, controller, callback)
		{
			if (Routing.pages[pageId] === undefined) {
				return false;
			}
			if (controller !== undefined && Routing.controller[controller]) {
				Routing.controller[controller](function() {
					switchToPage(Routing.pages[Routing.currentPage], Routing.pages[pageId]);
				});
			} else {
				switchToPage(Routing.pages[Routing.currentPage], Routing.pages[pageId]);
			}
		}

		// --------------------------------------------------------------------------
		// Controllers
		// --------------------------------------------------------------------------

		Routing.controller.HomepageController = function(callback, options)
		{
			if (Routing.step.hasClass('btn-fb-login')) {
				DOMHelper.showLoader();

				$('.btn-fb-login').off('click').on('click', function(e){
					e.stopPropagation();
					e.preventDefault();
				});

				FBH.tryFacebookConnect(function() {
					ga('send', "event", "Facebook", "Homepage");
					ga('send', "pageview", "/" + appConfig.culture + "/facebook-connect");
					$('.page-2 .ul-list').html(DOMHelper.getRoutesHTML('routes'));
					DOMHelper.hideLoader();
					callback();
				});
			} else {
				ga('send', "event", "NoLogin", "Homepage");
				callback();
			}
		};

		Routing.controller.MapsConnectController = function(callback, options)
		{
			DOMHelper.showLoader();
			FBH.tryFacebookConnect(function() {
				ga('send', "event", "Facebook", "Maps");
				if (FBH.User.id !== null) {
					var mapsPage = $('.page-3');
					mapsPage.find('#fb-connect-button').remove();
					mapsPage.find('.friend-list').html(DOMHelper.getUserFriendsHTML());
				}
				DOMHelper.hideLoader();
				callback();
			});
		};

		Routing.controller.RidePlaybackController = function(callback, options)
		{
			DOMHelper.hideMap();
			r4.utils.header('off');

			if (appConfig.ride.points.length == 2)
			{
				Flash.getDirections({
					from: [appConfig.ride.points[0][0], appConfig.ride.points[0][1]],
					to: [appConfig.ride.points[1][0], appConfig.ride.points[1][1]],
					target: appConfig.ride.target,
					speed: appConfig.ride.speed,
					effectID: appConfig.ride.effect,
					pitch: appConfig.ride.pitch,
					sound: r4.utils.audio.isPlaying
				}, _callback);
			}
			else
			{
				Flash.callSetDirections({
					points: appConfig.ride.points,
					target: appConfig.ride.target,
					speed: appConfig.ride.speed,
					effectID: appConfig.ride.effect,
					pitch: appConfig.ride.pitch,
					sound: r4.utils.audio.isPlaying
				});
				_callback();
			}

			function _callback()
			{
				ga('send', "event", "Playback");
				return callback();
			}
		};

		Routing.controller.GetGoingController = function(callback, options)
		{
			DOMHelper.hideMap();
			r4.utils.header('off');

			var id = (options.page).parents('li:eq(0)').data('route-id'),
				rl, flashOk;

			var target = DOMHelper.routeList[id].GPS.target;
			if (target === undefined) {
				target = [0,0];
			}

			if(DOMHelper.routeList[id].GPS) {
				Flash.getDirections({
					from: [DOMHelper.routeList[id].GPS.fromLat, DOMHelper.routeList[id].GPS.fromLng],
					to: [DOMHelper.routeList[id].GPS.toLat, DOMHelper.routeList[id].GPS.toLng],
					target: target,
					pitch: DOMHelper.routeList[id].pitch,
					effectID: DOMHelper.routeList[id].effect,
					speed: DOMHelper.routeList[id].speed,
					sound: r4.utils.audio.isPlaying
				}, _getGoing);
			} else {
				_getGoing();
			}

			function _getGoing(routeLength)
			{
				var points;
				if($.isArray(DOMHelper.routeList[id].points)) {
					Flash.callSetDirections({
						points: DOMHelper.routeList[id].points,
						target: target,
						pitch: DOMHelper.routeList[id].pitch,
						effectID: DOMHelper.routeList[id].effect,
						speed: DOMHelper.routeList[id].speed,
						sound: r4.utils.audio.isPlaying
					});
					points = DOMHelper.routeList[id].points;
				} else {
					points = [
						[DOMHelper.routeList[id].GPS.fromLat, DOMHelper.routeList[id].GPS.fromLng],
						[DOMHelper.routeList[id].GPS.toLat, DOMHelper.routeList[id].GPS.toLng]
					];
				}
				if(r4.cache.footer.find('.sound-on').hasClass('act')) {
					Flash.flashDOM.mute();
				}
				if(r4.cache.footer.find('.sound-off').hasClass('act')) {
					Flash.flashDOM.unmute();
				}

				var _d = $('<div/>').html(SkodaBackendConfig.i18n["route_"+(id+1)]);
				ga('send', "event", "Get going", _d.text());
				ga('send', "event", "Route length", Math.ceil(routeLength / 1000));
				ga('send', "pageview", "/" + appConfig.culture + "/play-video");
				if (Skoda.FBH.getFriendsArray().length > 0) {
					ga('send', "event", "FBFriends", Skoda.FBH.getFriendsArray().length);
				}

				FBH.saveRide({
					name: FBH.User.name,
					id: FBH.User.id,
					gender: FBH.User.gender,
					points: points,
					target: target,
					speed: DOMHelper.routeList[id].speed,
					pitch: DOMHelper.routeList[id].pitch,
					effectId: DOMHelper.routeList[id].speed,
					length: Math.ceil(routeLength / 1000)
				}, appConfig.culture, function(response) {
					FBH.User.lastRide = {
						id: DOMHelper.routeList[id].id,
						hash: JSON.parse(response).hash
					};
				});

				return callback();
			}
		};

		// --------------------------------------------------------------------------
		// Events
		// --------------------------------------------------------------------------

		Routing.events.init[1] = function(callback) {

			r4.cache.header.addClass('act');
			setTimeout(function(){
          r4.cache.header.removeClass('act');
      }, 5000);

			// Show the video
			$('.video').css('visibility', 'visible');

			return callback();
		};

		Routing.events.exit[1] = function(callback) {

			r4.cache.header.removeClass('act');

			// Destroy video
      r4.cache.homevideo.dispose();

			// Remove the video from HP
			$('.video').remove();

			// Play / pause audio
			var a = document.createElement('audio');
			if((!!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''))) || (!!(a.canPlayType && a.canPlayType('audio/wav; codecs="1"').replace(/no/, '')))) {
				r4.utils.audio.setObject(r4.cache.audio);
				if (r4.utils.audio.isPlaying) {
					r4.utils.audio.play();
				} else {
					r4.utils.audio.pause();
				}
			}

			return callback();
		};

		Routing.events.init[2] = function(callback) {
			if (r4.cache.sly) {
				DOMHelper.changeTestDrive();
			}

			// List animate
			$('.ul-list').addClass('animate');

			return callback();
		};

		Routing.events.exit[2] = function(callback) {
			// List animate
			$('.ul-list').removeClass('animate');

			return callback();
		};

		Routing.events.init[3] = function(callback) {
			r4.utils.header('off');
			var mapsPage = $('.page-3');
			if (FBH.User.id === null) {
				mapsPage.find('.map-friends').hide();
			} else {
				mapsPage.find('.map-friends').show();
				if (mapsPage.find('.friend-list').find('li').length === 0) {
					mapsPage.find('.friend-list').html(DOMHelper.getUserFriendsHTML());
				}
				mapsPage.find('#fb-connect-button').hide();
			}
			return callback();
		};

		Routing.events.exit[3] = function(callback) {
			r4.utils.audio.isFlashPlaying = false;
			DOMHelper.removeFlash();
			DOMHelper.embedFlash(function() {
				r4.utils.header('on');
				if (!Map.isCustomRouteBeingCreated()) {
					$('.maps-help').show();
				}
				$('.maps-aside, #map-container').show();
			});

			return callback();
		};

		Routing.events.init[4] = function(callback) {
			$('.page-4 .ul-list').html(Skoda.DOMHelper.getRoutesHTML('after-video'));
			twttr.widgets.load();
			return callback();
		};

		Routing.events.exit[4] = function(callback) {
			$('.page-5 .btn-back').show();
			return callback();
		};

		return {
			navigate: navigate
		};

	})();

	// --------------------------------------------------------------------------
	// Map helper
	// --------------------------------------------------------------------------

	var Map = (function() {

		var customRouteIsBeingCreated = false,
			moods = {
				0: [0,7], // Retro
				1: [1,8], // Hip hop
				2: [2,9,12], // Pop
				3: [4,11], // Chillout
				4: [5,14], // House
				5: [6,13], // Dubstep
				6: [3,10] // Rock
			};

		function setCustomRouteBeingCreated(state) {
			customRouteIsBeingCreated = state;
			if (state === false) {
				Map.clear();
			}
			return state;
		}

		function isCustomRouteBeingCreated() {
			return customRouteIsBeingCreated;
		}

		function clear()
		{
			if (_startMarker) {
				_startMarker.setMap(null);
			}
			if (_endMarker) {
				_endMarker.setMap(null);
			}
			_startPoint = null;
			_endPoint = null;
			_streetViewLock = false;

			var i,l;
			for (i=0,l=_circles.length;i<l;i++) {
				_circles[i].setMap(null);
			}
			_circles = [];
		}

		return {
			isCustomRouteBeingCreated: isCustomRouteBeingCreated,
			setCustomRouteBeingCreated: setCustomRouteBeingCreated,
			clear: clear,
			moods: moods
		};
	})();

	// --------------------------------------------------------------------------
	// Flash helper
	// --------------------------------------------------------------------------

	var Flash = (function() {

		var flashDOM;

		function initialized() {
			Flash.flashDOM = document.getElementById('flashcontainer');
		}

		function muteBackgroundMusic() {
			// Please DO stop the music music music ...
			r4.utils.audio.isFlashPlaying = true;
			r4.utils.audio.pause();
		}

		function roadFinished() {
			r4.utils.audio.isFlashPlaying = false;

			// Play audio
			if (r4.cache.footer.find('.sound-off').hasClass('act')) {
				r4.utils.audio.play();
			}
			Skoda.Router.navigate(4);
		}

		function callSetDirections(params) {
			var _friends = FBH.getFriendsArray();
			Flash.flashDOM.setDirections(params.points, params.target, params.speed, params.pitch, params.effectID, _friends, params.sound);
		}

		function getDirections(params, callback) {
			var routeLength = 0,
				request = {
					origin: new google.maps.LatLng(params.from[0], params.from[1]),
					destination: new google.maps.LatLng(params.to[0], params.to[1]),
					travelMode: google.maps.TravelMode.DRIVING
				};

			_directionsService.route(request, function(response, status) {
				if (status != google.maps.DirectionsStatus.OK) {
					Skoda.DOMHelper.showMapsError('maps_error_bad_points');
					Skoda.Map.clear();
					return;
				}

				Flash.parsePoints(response);
				routeLength = response.routes[0].legs[0].distance.value;

				var route = response.routes[0].overview_path;
				for (var i = 0; i < route.length; i++) {
					route[i] = [route[i].lat(), route[i].lng()];
				}

				try {
					var _friends = FBH.getFriendsArray();
					Flash.flashDOM.setDirections(route, params.target, params.speed, params.pitch, params.effectID, _friends, params.sound);
					if (callback !== undefined && typeof callback === "function") {
						return callback(routeLength);
					}
				} catch(e) {
					console.log(e);
				}
			});
		}

		function parsePoints(response) {
			var route = response.routes[0];
			var path = route.overview_path;
			var legs = route.legs;
			var raw_points = [];
			var _d = 20;

			var total_distance = 0;
			for (var i = 0; i < legs.length; ++i) {
				total_distance += legs[i].distance.value;
			}

			var segment_length = total_distance / 300;
			_d = (segment_length < 20) ? _d = 20 : _d = segment_length;

			var d = 0;
			var r = 0;
			var a, b;

			for (i = 0; i < path.length; i++) {
				if (i + 1 < path.length) {

					a = path[i];
					b = path[i + 1];
					d = google.maps.geometry.spherical.computeDistanceBetween(a, b);

					if (r > 0 && r < d) {
						a = pointOnLine(r / d, a, b);
						d = google.maps.geometry.spherical.computeDistanceBetween(a, b);
						raw_points.push(a);

						r = 0;
					} else if (r > 0 && r > d) {
						r -= d;
					}

					if (r === 0) {
						var segs = Math.floor(d / _d);

						if (segs > 0) {
							for (var j = 0; j < segs; j++) {
								var t = j / segs;

								if (t > 0 || (t + i) === 0) { // not start point
									var way = pointOnLine(t, a, b);
									raw_points.push(way);
								}
							}

							r = d - (_d * segs);
						} else {
							r = _d * (1 - (d / _d));
						}
					}

				} else {
					raw_points.push(path[i]);
				}
			}

			response.routes[0].overview_path = raw_points;
		}

		return {
			flashDOM: flashDOM,
			initialized: initialized,
			roadFinished: roadFinished,
			callSetDirections: callSetDirections,
			getDirections: getDirections,
			parsePoints: parsePoints,
			muteBackgroundMusic: muteBackgroundMusic
		};
	})();

	var initialize = (function()
	{
		DOMHelper.embedFlash();
		DOMHelper.registerEvents();
		if (appConfig.ride !== undefined) {
			DOMHelper.embedFlash(function() {
				r4.utils.audio.setObject(r4.cache.audio);
				// Wait for Google Maps directions service and then go
				var waitForMapsMaps = window.setInterval(function() {
					if (!!document.getElementById('map').firstChild && Flash.flashDOM !== undefined) {
						window.clearInterval(waitForMapsMaps);
						Router.navigate(3, 'RidePlaybackController');
					}
				}, 100);
			});
		} else {
			Router.navigate(1);
		}
	})();

	return {
		FBH: FBH,
		DOMHelper: DOMHelper,
		Router: Router,
		Map: Map,
		Flash: Flash
	};

})($, r4, SkodaBackendConfig);

// --------------------------------------------------------------------------
// Flash helpers and stuff
// --------------------------------------------------------------------------

	// Flash stuff

	var MAX_DISTANCE = 30000,
		_directionsService,
		_directionsDisplay,
		_streetViewService,
		_map,
		_flash,
		_startPoint,
		_endPoint,
		_startMarker,
		_endMarker,
		_streetViewLock,
		_panoClient,
		_circles = [];

	// ---

	var initialize = function () {

		var mapOptions = {
			zoom: 11,
			center: new google.maps.LatLng(window.appConfig.gmaps.lat, window.appConfig.gmaps.lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			scaleControl: false,
			overviewMapControl: false,
			streetViewControl: false,
            panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			streetViewControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			}
		};

		_map = new google.maps.Map(document.getElementById('map'), mapOptions);
		_map.setOptions({
			styles: [{
				"featureType": "water",
				"stylers": [{
					"hue": "#0091ff"
				}, {
					"saturation": -100
				}, {
					"lightness": -11
				}]
				}, {
				"featureType": "administrative",
				"stylers": [{
					"hue": "#00ffe6"
				}, {
					"weight": 0.1
				}, {
					"visibility": "on"
				}]
				}, {
				"featureType": "landscape.natural.terrain",
				"stylers": [{
					"visibility": "off"
				}]
				}, {
				"featureType": "road",
				"stylers": [{
					"visibility": "simplified"
				}, {
					"saturation": -56
				}, {
					"hue": "#08ff00"
				}]
				}, {
				"featureType": "administrative.country",
				"stylers": [{
					"visibility": "off"
				}]
				}
			]
		});

		var _streetViewLayer = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				var X = coord.x % (1 << zoom);
				return "http://cbk0.google.com/cbk?output=overlay&zoom=" + zoom + "&x=" + X + "&y=" + coord.y + "&cb_client=api";
			},
			tileSize: new google.maps.Size(256, 256),
			isPng: true
		});

		_directionsService = new google.maps.DirectionsService();
		_directionsDisplay = new google.maps.DirectionsRenderer();
		_directionsDisplay.setMap(_map);
		_streetViewService = new google.maps.StreetViewService();
		_panoClient = new google.maps.StreetViewService();

		google.maps.event.addListener(_map, 'click', function (e) {

			if (_streetViewLock) {
				return;
			}

			_streetViewLock = true;

			// get nearest road
			var request = {
				origin: e.latLng,
				destination: e.latLng,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

			google.maps.Circle.prototype.contains = function(latLng) {
              return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
            };

			_directionsService.route(request, function (data, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					// got the road, check for streetview
					_streetViewService.getPanoramaByLocation(data.routes[0].legs[0].start_location, 500, function (data, status) {
						if (status == 'OK') {

							if (!_startPoint) {
								_startPoint = data.location.latLng;
								_startMarker = new google.maps.Marker({
									position: _startPoint,
									map: _map
								});

								_circles.push(new google.maps.Circle({
									map: _map,
									center: new google.maps.LatLng(_startPoint.lat(),_startPoint.lng()),
									strokeColor: "#35c907",
									strokeOpacity: 1,
									strokeWeight: 0.4,
									fillColor: "#179805",
									fillOpacity: 0.5,
									clickable: false,
									radius: 919
								}));

								_circles.push(new google.maps.Circle({
									map: _map,
									center: new google.maps.LatLng(_startPoint.lat(),_startPoint.lng()),
									strokeColor: "#FFFFFF",
									strokeOpacity: 1,
									strokeWeight: 1,
									fillColor: "#179805",
									fillOpacity: 0,
									clickable: false,
									radius: 9160
								}));

								_circles.push(new google.maps.Circle({
									map: _map,
									center: new google.maps.LatLng(_startPoint.lat(),_startPoint.lng()),
									strokeWeight: 0,
									fillColor: "#35c907",
									fillOpacity: 0.3,
									clickable: false,
									radius: 18362.55489862987
								}));

								_streetViewLock = false;

								return;
							}

							if (!_endPoint) {
								// if in radius

								if(_circles[0].contains(data.location.latLng)){
									_streetViewLock = false;
									Skoda.DOMHelper.showMapsError('maps_error_further_destination');
								} else if(!_circles[2].contains(data.location.latLng)){
									_streetViewLock = false;
									Skoda.DOMHelper.showMapsError('maps_error_closer_destination');
								} else {
									_endPoint = data.location.latLng;
									_endMarker = new google.maps.Marker({
										position: _endPoint,
										map: _map
									});

									var _moo = $('#moodselector').val(),
										_effect;
									if (!_moo) {
										var __m = Math.floor(Math.random() * 7);
										_effect = Skoda.Map.moods[__m][Math.floor(Math.random() * Skoda.Map.moods[__m].length)];
									} else {
										_effect = Skoda.Map.moods[_moo][Math.floor(Math.random() * Skoda.Map.moods[_moo].length)];
									}

									Skoda.Flash.getDirections({
										from: [_startPoint.lat(), _startPoint.lng()],
										to: [_endPoint.lat(), _endPoint.lng()],
										target: [0, 0],
										pitch: 0,
										effectID: _effect,
										speed: 7,
										sound: r4.utils.audio.isPlaying
									}, function(routeLength) {

										ga('send', "event", "Journey", "Custom journey");
										ga('send', "event", "Route length", Math.ceil(routeLength / 1000));
										ga('send', "event", "FBFriends", Skoda.FBH.getFriendsArray().length);

										Skoda.DOMHelper.hideMap();
										Skoda.FBH.saveRide({
											name: Skoda.FBH.User.name,
											id: Skoda.FBH.User.id,
											gender: Skoda.FBH.User.gender,
											points: [[_startPoint.lat(), _startPoint.lng()], [_endPoint.lat(), _endPoint.lng()]],
											target: [0, 0],
											speed: 7,
											pitch: 0,
											effectId: _effect,
											length: Math.ceil(routeLength / 1000),
										}, appConfig.culture, function(response) {
											Skoda.FBH.User.lastRide = {
												id: null,
												hash: JSON.parse(response).hash
											};
										});

										Skoda.Map.setCustomRouteBeingCreated(false);
									});
								}
							}
						} else {
							Skoda.DOMHelper.showMapsError('maps_error_no_streetview');
							_streetViewLock = false;
						}
					});
				} else {
					Skoda.DOMHelper.showMapsError('maps_error_no_roads');
					_streetViewLock = false;
				}
			});

		});

		var input = document.getElementById('searchBox');
        var searchBox = new google.maps.places.SearchBox(input);
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            var place = searchBox.getPlaces();
            if (place[0].geometry) {
                var location = place[0].geometry.location;

                if(place[0].geometry.viewport){
                    _map.fitBounds(place[0].geometry.viewport);
                }else{
                    _map.panTo(location);
                    _map.setZoom(16);
                }
                _map.overlayMapTypes.setAt(0, _streetViewLayer);

                input.value = "";
            }
        });

        _map.overlayMapTypes.setAt(0, _streetViewLayer);
	};

	var distanceBetween = function (a, b) {
		var aLL = new google.maps.LatLng(a[0], a[1]);
		var bLL = new google.maps.LatLng(b[0], b[1]);
		return google.maps.geometry.spherical.computeDistanceBetween(aLL, bLL);
	};

	var pointOnLine = function (percent, a, b) {
		var lat1 = a.lat();
		var lon1 = a.lng();
		var lat2 = b.lat();
		var lon2 = b.lng();
		return new google.maps.LatLng(lat1 + percent * (lat2 - lat1), lon1 + percent * (lon2 - lon1));
	};

	google.maps.visualRefresh = true;
	google.maps.event.addDomListener(window, 'load', initialize);
