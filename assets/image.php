<?php

	/**
	 * Image downloader for the hyperlapse
	 * @author Symbio
	 * @package Skoda-Rapid-Spaceback
	 */

	// Encode a string to URL-safe base64
	function encodeBase64UrlSafe($value)
	{
	  return str_replace(array('+', '/'), array('-', '_'), base64_encode($value));
	}

	// Decode a string from URL-safe base64
	function decodeBase64UrlSafe($value)
	{
	  return base64_decode(str_replace(array('-', '_'), array('+', '/'), $value));
	}

	// Sign a URL with a given crypto key
	// Note that this URL must be properly URL-encoded
	function signUrl($myUrlToSign, $privateKey)
	{
	  // parse the url
	  $url = parse_url($myUrlToSign);
	  $urlPartToSign = $url['path'] . "?" . $url['query'];

	  // Decode the private key into its binary format
	  $decodedKey = decodeBase64UrlSafe($privateKey);

	  // Create a signature using the private key and the URL-encoded
	  // string using HMAC SHA1. This signature will be binary.
	  $signature = hash_hmac("sha1",$urlPartToSign, $decodedKey,  true);

	  $encodedSignature = encodeBase64UrlSafe($signature);

	  return $myUrlToSign."&signature=".$encodedSignature;
	}

	// Get parameters from request
	$api_url = 'http://maps.googleapis.com/maps/api/streetview';
	$safe_params = array('size', 'location', 'fov', 'heading', 'pitch');
	$params = $_GET['params'];
	foreach ($params as $k => $param) {
		if (!in_array($k, $safe_params)) {
			unset($params[$k]);
		}
	}

	// Provide client id & hardcoded false sensor
	$params['client'] = 'gme-sinnerschrader';
	$params['sensor'] = 'false';

	// Build the request uri and sign it using cryptokey
	$image_url = $api_url . "?" . http_build_query($params);
	$url = signUrl($image_url, 'X1Hka-s1wEPtM4JSdU_saM4lGBs=');
	$sw = file_get_contents($url);

	header('Content-type: image/jpeg');

	if (isset($_GET['params']['mobile']))
	{
		$im = imagecreatefromstring($sw);
		$rgb = array(
			rand(0,50), rand(0,50), rand(0,50)
		);
		imagefilter($im, IMG_FILTER_COLORIZE, $rgb[0], $rgb[1], $rgb[2]);
		imagejpeg($im, null, 100);
	}
	else
	{
		echo $sw;
	}

	// Return the image
	exit;
