<?php

	namespace Skoda\Service;

	class FacebookService
	{

		/** @var Facebook PHP SDK */
		private $facebook;

		function __construct(\Facebook $facebook)
		{
			$this->facebook = $facebook;
		}

		/**
		 * Returns Facebook PHP SDK class for usage outside manager
		 * @return type
		 */
		public function getFacebook()
		{
			return $this->facebook;
		}

		public function publishOGAction($action, $postData)
		{
			return $this->facebook->api(
				'me/skoda-rapid-spacebk:' . $action,
				'POST',
				$postData
			);
		}

		/**
		 * Posts given message to users feed
		 * @param type $message
		 * @return type
		 */
		public function publishToFeed($message)
		{
			try
			{
				$this->facebook->api('/me/feed', 'POST', array(
					'link' => (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
					'message' => $message
				));

				return true;
			}
			catch (\Exception $e) {
				return false;
			}
		}

		/**
		 * Returns array containing user data from facebook
		 * @return type
		 */
		public function getUserFacebookInfo()
		{
			$fb_user = $this->facebook->api('/me');

			return array(
				"name" => $fb_user["name"],
				"id" => $fb_user["id"],
				"gender" => $fb_user["gender"],
				"friends" => $this->getUserFriends()
			);
		}

		/**
		 * Returns 4 random friends of user
		 * @return type
		 * @deprecated
		 */
		protected function getUserFriends()
		{
			$fb_friends = $this->facebook->api('/me?fields=friends.limit(300)');
			$fb_friends = $fb_friends['friends']['data'];

			$friends = array();
			if (count($fb_friends) > 4)
			{
				$used_ids = array();
				while (count($friends) < 4)
				{
					$idx = rand(0, count($fb_friends));
					$id = $fb_friends[$idx]['id'];
					if (!in_array($fb_friends[$idx]['id'], $used_ids)) {
						$friends[] = array('name' => $fb_friends[$idx]['name'], 'id' => (string) $fb_friends[$idx]['id']);
						$used_ids[] = $fb_friends[$idx]['id'];
					}
				}
			}
			else
			{
				foreach ($fb_friends as $friend) {
					$friends[] = array('name' => $friend['name'], 'id' => (string) $friend['id']);
				}
			}

			return $friends;
		}

		/**
		 * Returns all music genres from users music likes
		 * @return type
		 * @deprecated
		 */
		protected function getUserMusicGenres()
		{
			$fb_music = $this->facebook->api('/me?fields=music.limit(100)');
			$fb_music = $fb_music['music']['data'];

			$ids = array();
			foreach($fb_music as $artist) {
				$ids[] = $artist['id'];
			}

			$genresInfo = $this->facebook->api('/?ids=' . implode(',', $ids) . '&fields=genre');
			$music = array();
			foreach ($genresInfo as $artist) {
				if (isset($artist['genre'])) {
					if (!in_array($artist['genre'], $music)) {
						$music[$artist['genre']] = 1;
					} else {
						$music[$artist['genre']]++;
					}
				}
			}

			return $music;
		}

	}