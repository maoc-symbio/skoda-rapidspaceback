<?php

	namespace Skoda\Service;

	class i18nService
	{

		/** @var Default application culture used */
		protected $defaultCulture;

		/** @var Application current culture */
		protected $currentCulture;

		/** @var Array of domains and supported cultures */
		protected $domains;

		/** @var Array of supported cultures */
		protected $cultures;

		/** @var Array of countries for redirect */
		protected $countries;

		/** @var Array of regions */
		protected $regions;

		public function __construct($defaultCulture = "en", $config) {
			$this->defaultCulture = $defaultCulture;

			$this->cultures = $config['cultures'];
			$this->domains = $config['domains'];
			$this->countries = $config['countries'];
			$this->regions = $config['regions'];
		}

		public function resolveCulture($lang = NULL)
		{
			if ($lang === NULL) {
				$this->currentCulture = $this->defaultCulture;
			} else if (isset($this->cultures[$lang])) {
				$this->currentCulture = $lang;
			} else {
				$this->currentCulture = $this->defaultCulture;
			}

			return $this->currentCulture;
		}

		/**
		 * Returns parsed array of translations
		 * @return type
		 */
		public function getTranslations()
		{
			$file = ASSETS_DIR . '/i18n/messages.' . $this->getCulture() . '.xml';
			if (!is_file($file)) {
				throw new \Exception('Translation file for culture ' . $this->getCulture() . ' not found at ' . $file . '.');
			}

			$data = array();
			$xml = simplexml_load_file($file);
			$messages = $xml->xpath('/messages/message');

			foreach($messages as $msg) {
				$data[(string) $msg->key] = (string) $msg->value;
			}

			return $data;
		}

		public function getCultureInfo()
		{
			$data = $this->cultures[$this->currentCulture];
			return array_merge($data, array(
				"culture" => $this->currentCulture,
				"country" => isset($this->cultures[$this->currentCulture]['country']) ? $this->cultures[$this->currentCulture]['country'] : $this->currentCulture
			));
		}

		/**
		 * Current culture
		 * @return string
		 */
		public function getCulture() {
			return $this->currentCulture;
		}

		/**
		 * Array of domains for redirects
		 * @return array
		 */
		public function getDomains() {
			return $this->domains;
		}

		/**
		 * Array of allowed languages for the website mutation
		 * @return array
		 */
		public function getCultures() {
			$ret = array();
			foreach ($this->cultures as $culture => $data) {
				$d = $data;
				$d["culture"] = $culture;
				$ret[$culture] = $d;
			}
			return $ret;
		}

		public function getCountries() {
			return $this->countries;
		}

		public function getRegions() {
			return $this->regions;
		}

	}