<?php

	namespace Skoda\Service;

	use WindowsAzure\Table\Models\Entity;
	use WindowsAzure\Table\Models\EdmType;

	class StatsManager
	{

		/** @var AzureService */
		private $azureService;

		private $i18nService;

		/** @var Azure Tables table name */
		private $storageTableName = 'statistics';

		const TYPE_USER = 1,
			TYPE_COUNTRY = 2,
			TYPE_REGION = 3,
			TYPE_WORLDWIDE = 4,
			TYPE_TOP5 = 5,
			TYPE_RIDE = 6;

		function __construct(\Skoda\Service\AzureService $azureService, $i18nService) {
			$this->azureService = $azureService;
			$this->i18nService = $i18nService;
		}

		protected function createEntity($type = self::TYPE_USER)
		{
			$entity = $this->azureService->createEntity();
			$entity->addProperty("Type", EdmType::INT32, $type);
			return $entity;
		}

		public function saveUserStatistics($userId, $value, $culture, $region)
		{
			$proxy = $this->azureService->getTableRestProxy();
			$entities = $proxy->queryEntities($this->storageTableName, 'Type eq '.self::TYPE_USER.' and Info eq \''.$userId.'\'')->getEntities();

			try
			{

				if (!isset($entities[0]))
				{
					$entity = $this->createEntity();
					$entity->addProperty("Info", EdmType::STRING, $userId);
					$entity->addProperty("Value", EdmType::INT32, (int) round($value));
					$proxy->insertEntity($this->storageTableName, $entity);
				}
				else
				{
					$entity = $entities[0];
					$entity->setPropertyValue("Value", $entity->getPropertyValue("Value") + (int) round($value));
					$proxy->updateEntity($this->storageTableName, $entity);
				}

				$this->saveGeoStatistics(self::TYPE_COUNTRY, $culture, $value);
				$this->saveGeoStatistics(self::TYPE_REGION, $region, $value);
				$this->saveGeoStatistics(self::TYPE_WORLDWIDE, "ww", $value);

				$this->updateTopFiveStatistics($userId, $entity->getPropertyValue("Value"));

				return true;

			} catch (\Exception $e) {
				return false;
			}

		}

		public function saveGeoStatistics($type, $info = NULL, $value)
		{
			$proxy = $this->azureService->getTableRestProxy();
			$entities = $proxy->queryEntities($this->storageTableName, 'Type eq '.$type.' and Info eq \''.$info.'\'')->getEntities();

			if (!isset($entities[0])) {
				throw new \Exception("Entity missing - application was'nt initalized properly.");
			}

			try
			{
				$entity = $entities[0];
				$entity->setPropertyValue("Value", $entity->getPropertyValue("Value") + (int) round($value));
				$proxy->updateEntity($this->storageTableName, $entity);
				return true;

			} catch (\Exception $e) {
				return false;
			}
		}

		protected function updateTopFiveStatistics($userId, $value)
		{
			if (!is_numeric($userId)) {
				return;
			}

			$proxy = $this->azureService->getTableRestProxy();
			$entities = $proxy->queryEntities($this->storageTableName, 'Type eq '.self::TYPE_TOP5)->getEntities();
			$entity = $entities[0];

			$top5 = unserialize($entity->getPropertyValue('Info'));

			if (count($top5) == 5 && $value > end($top5))
			{
				$top5[$userId] = $value;
				arsort($top5, SORT_NUMERIC);
				array_pop($top5);
			}
			else if (count($top5) < 5)
			{
				if ((isset($top5[$userId]) && $value > $top5[$userId]) || !isset($top5[$userId])) {
					$top5[$userId] = $value;
					arsort($top5, SORT_NUMERIC);
				}
			}
			else
			{
				return;
			}

			$entity->setPropertyValue("Info", serialize($top5));
			$proxy->updateEntity($this->storageTableName, $entity);
		}

		public function getStatistics($cultureInfo)
		{
			$r = array(
				"top5" => $this->getTopFiveStatistics(),
				"geo" => $this->getGeoStatistics($cultureInfo),
				"facts" => $this->getFactsStatistics(),
			);
			return $r;
		}

		private function getTopFiveStatistics() {
			$proxy = $this->azureService->getTableRestProxy();
			$entities = $proxy->queryEntities($this->storageTableName, 'Type eq '.self::TYPE_TOP5)->getEntities();
			$entity = $entities[0];

			$top5Array = unserialize($entity->getPropertyValue('Info'));
			$vals = array_values($top5Array);
			$topDriver = array_shift($vals);

			$ret = array();

			$users = json_decode(file_get_contents('https://graph.facebook.com/?ids=' . implode(',', array_keys($top5Array))), true);
			foreach ($top5Array as $uid => $value) {
				$ret[] = array(
					'name' => $users[$uid]['name'],
					'percentage' => round($value / ($topDriver / 100)),
					'km' => $value
				);
			}
			return $ret;
		}

		private function getGeoStatistics($cultureInfo) {
			$proxy = $this->azureService->getTableRestProxy();
			$ret = array();

			$q = $proxy->queryEntities($this->storageTableName, 'Type eq ' . self::TYPE_COUNTRY . ' and Info eq \'' . $cultureInfo['country'] . '\'')->getEntities();
			$e = $q[0];
			$ret['culture'] = number_format($e->getPropertyValue('Value'), 0, " ", " ");

			$q = $proxy->queryEntities($this->storageTableName, 'Type eq ' . self::TYPE_REGION . ' and Info eq \''. $cultureInfo["region"] .'\'')->getEntities();
			$e = $q[0];
			$ret['region'] = number_format($e->getPropertyValue('Value'), 0, " ", " ");

			$q = $proxy->queryEntities($this->storageTableName, 'Type eq ' . self::TYPE_WORLDWIDE)->getEntities();
			$e = $q[0];
			$ret['world'] = number_format($e->getPropertyValue('Value'), 0, " ", " ");

			return $ret;
		}

		public function getFactsStatistics() {
			$stats = array();
			$used_stats = array();
			$t = $this->i18nService->getTranslations();
			while (count($stats) < 2) {
				$st = rand(1,6);
				if (!in_array($st, $used_stats)) {
					$stat = $t['stat_' . $st];
					if (preg_match('/(\d+)\ \%/', $stat, $m)) {
						$stats[] = array("percentage" => $m[1], "text" => $stat);
					} else {
						$stats[] = array("percentage" => 0, "text" => $stat);
					}
					$used_stats[] = $st;
				}
			}
			return $stats;
		}

	}