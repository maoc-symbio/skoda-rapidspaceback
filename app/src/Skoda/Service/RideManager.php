<?php

	namespace Skoda\Service;

	use WindowsAzure\Table\Models\Entity;
	use WindowsAzure\Table\Models\EdmType;

	class RideManager
	{

		/** @var AzureService */
		private $azureService;

		/** @var Statistics manager */
		private $statsManager;

		private $storageTableName = 'rides';

		function __construct(\Skoda\Service\AzureService $azureService, \Skoda\Service\StatsManager $statsManager) {
			$this->azureService = $azureService;
			$this->statsManager = $statsManager;
		}

		public function save($ride, $culture, $region)
		{
			$entity = $this->azureService->createEntity();

			$entity->addProperty("name", EdmType::STRING, $ride['name']);
			$entity->addProperty("facebookId", EdmType::STRING, $ride['facebookId']);
			$entity->addProperty("gender", EdmType::STRING, $ride['gender']);
			$entity->addProperty("points", EdmType::STRING, $ride['points']);
			$entity->addProperty("target", EdmType::STRING, $ride['target']);
			$entity->addProperty("culture", EdmType::STRING, $ride['culture']);
			$entity->addProperty("speed", EdmType::INT32, (int) $ride['speed']);
			$entity->addProperty("pitch", EdmType::INT32, (int) $ride['pitch']);
			$entity->addProperty("effect", EdmType::INT32, (int) $ride['effect']);

			if (empty($ride['name']) || empty($ride['facebookId'])) {
				$str = sha1(uniqid().time());
			} else {
				$str = $ride['name'].$ride['facebookId'].time();
			}
			$hash = substr(md5($str), 0, 18);
			$entity->addProperty('hash', EdmType::STRING, $hash);

			try {
				$res = $this->azureService->insertEntity($this->storageTableName, $entity);
				$this->statsManager->saveUserStatistics($ride['facebookId'], $ride['length'], $culture, $region);
				return array("hash" => $hash, "culture" => $culture);
			} catch (Exception $e) {
				return false;
			}
		}

		public function findRideByHash($hash)
		{
			$proxy = $this->azureService->getTableRestProxy();
			$res = $proxy->queryEntities($this->storageTableName, 'hash eq \''.$hash.'\'')->getEntities();

			if (empty($res)) {
				return false;
			}
			return $res[0];
		}

	}