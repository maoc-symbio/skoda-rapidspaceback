<?php

	namespace Skoda\Service;

	use WindowsAzure\Common\ServicesBuilder;
	use WindowsAzure\Common\ServiceException;
	use WindowsAzure\Table\Models\Entity;
	use WindowsAzure\Table\Models\EdmType;

	class AzureService
	{
		/* @var Table REST proxy */
		private $tableRestProxy;

		function __construct($connectionString)
		{
			$this->tableRestProxy = ServicesBuilder::getInstance()->createTableService($connectionString);
		}

		public function getTableRestProxy()
		{
			return $this->tableRestProxy;
		}

		public function insertEntity($table, \WindowsAzure\Table\Models\Entity $entity)
		{
			return $this->tableRestProxy->insertEntity($table, $entity);
		}

		public function createEntity()
		{
			$entity = new Entity();
			$entity->setPartitionKey('p1');
			$entity->setRowKey((string) microtime(true));
			return $entity;
		}

		public function initialize($data)
		{
			/*
			$rideStat = $this->createEntity();
					$rideStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_COUNTRY);
					$rideStat->addProperty("Info", EdmType::STRING, 'it');
					$rideStat->addProperty("Value", EdmType::INT32, 0);
					$this->tableRestProxy->insertEntity("statistics", $rideStat);
			die(); */

			try {
				echo "Creating table 'rides'<br>";
				$this->tableRestProxy->createTable('rides');

				echo "Creating table 'statistics'<br>";
				$this->tableRestProxy->createTable('statistics');

				// Create empty entities for countries, regions and one for worldwide statistic
				echo "Creating empty entites for cultural statistics<br>";
				foreach ($data["cultures"] as $culture)
				{
					$countryStat = $this->createEntity();
					$countryStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_COUNTRY);
					$countryStat->addProperty("Info", EdmType::STRING, $culture);
					$countryStat->addProperty("Value", EdmType::INT32, 0);
					$this->tableRestProxy->insertEntity("statistics", $countryStat);
				}
				echo "OK<br>";

				echo "Creating empty entites for ride statistics<br>";
				for ($i=1;$i<18;$i++)
				{
					$rideStat = $this->createEntity();
					$rideStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_RIDE);
					$rideStat->addProperty("Info", EdmType::STRING, (string) $i);
					$rideStat->addProperty("Value", EdmType::INT32, 0);
					$this->tableRestProxy->insertEntity("statistics", $rideStat);
				}
				echo "OK<br>";

				echo "Creating empty entites for regional statistics<br>";
				foreach($data["regions"] as $region)
				{
					$regionStat = $this->createEntity();
					$regionStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_REGION);
					$regionStat->addProperty("Info", EdmType::STRING, $region);
					$regionStat->addProperty("Value", EdmType::INT32, 0);
					$this->tableRestProxy->insertEntity("statistics", $regionStat);
				}
				echo "OK<br>";

				echo "Creating empty entites for worldwide statistics<br>";
				$wwStat = $this->createEntity();
				$wwStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_WORLDWIDE);
				$wwStat->addProperty("Info", EdmType::STRING, "ww");
				$wwStat->addProperty("Value", EdmType::INT32, 0);
				$this->tableRestProxy->insertEntity("statistics", $wwStat);
				echo "OK<br>";

				echo "Creating empty TOP5 statistics<br>";
				$wwStat = $this->createEntity();
				$wwStat->addProperty("Type", EdmType::INT32, StatsManager::TYPE_TOP5);
				$wwStat->addProperty("Info", EdmType::STRING, serialize(array()));
				$wwStat->addProperty("Value", EdmType::INT32, 0);
				$this->tableRestProxy->insertEntity("statistics", $wwStat);
				echo "<strong>OK</strong><br>";


			} catch (\Exception $e) {
				echo $e->getMessage();
				echo '<strong>Terminating. There was an unexpected error during initialization or the application is already initialized.</strong><br>';
			}
		}


	}