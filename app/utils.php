<?php

	use Symfony\Component\HttpFoundation\Request,
		Symfony\Component\HttpFoundation\Response,
		Silex\Application;

	$app->get('/translate/{lang}', function($lang) use ($app)
	{
		header("Content-type: text/html;charset=utf-8");
		$dom = new DOMDocument();
		$dom->loadXML(file_get_contents(__DIR__ . '/../assets/i18n/messages.en.xml'));
		$translations = file_get_contents(__DIR__ . '/../assets/translations/' . $lang . '.csv');
		$messages = $dom->getElementsByTagName('message');
		foreach (explode("\n", $translations) as $t) {
			$tt = explode(";", $t);
			if (isset($tt[2]) && !empty($tt[0])) {
				foreach ($messages as $msg) {
					if ($msg->childNodes->item(1)->nodeValue == $tt[0])
					{
						$cm = $msg->childNodes->item(3)->ownerDocument->createCdataSection($tt[2]);
						$msg->childNodes->item(3)->nodeValue = "";
						$msg->childNodes->item(3)->appendChild($cm);
					}
				}
			}
		}
		return $dom->saveXML();
	});