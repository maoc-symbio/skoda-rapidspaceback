<?php

	$config = array();

	// --------------------------------------------------------------------------
	// Environment and services configuration
	// --------------------------------------------------------------------------

	include 'production.php';

	$config['application'] = array(
		'cultures' => array(
			'es' => array('gmap_lat' => '40.296287', 'gmap_lng' => '-2.285156', 'region' => 'eu', 'label' => 'España', 'active' => 1),
			'at' => array('gmap_lat' => '50.415218', 'gmap_lng' => '14.913940', 'region' => 'eu', 'label' => 'Österreich', 'active' => 1),
			'be' => array('gmap_lat' => '50.848137', 'gmap_lng' => '4.348145', 'region' => 'eu', 'label' => 'België', 'active' => 1),
			'be-fr' => array('gmap_lat' => '50.848137', 'gmap_lng' => '4.348145', 'region' => 'eu', 'label' => 'Belgique', 'country' => 'be', 'active' => 1),
			'sk' => array('gmap_lat' => '48.669026', 'gmap_lng' => '19.699024', 'region' => 'eu', 'label' => 'Slovensko', 'active' => 1),
			'cs' => array('gmap_lat' => '50.075538', 'gmap_lng' => '14.437800', 'region' => 'eu', 'label' => 'Česká republika', 'active' => 1),
			'ch' => array('gmap_lat' => '46.818188', 'gmap_lng' => '8.227512', 'region' => 'eu', 'label' => 'Schweiz', 'active' => 1),
			'ch-fr' => array('gmap_lat' => '46.818188', 'gmap_lng' => '8.227512', 'region' => 'eu', 'label' => 'Suisse', 'country' => 'ch', 'active' => 1),
			'ro' => array('gmap_lat' => '44.436553', 'gmap_lng' => '26.104889', 'region' => 'eu', 'label' => 'România', 'active' => 1),
			'pt' => array('gmap_lat' => '38.744076', 'gmap_lng' => '-9.184570', 'region' => 'eu', 'label' => 'Portugal', 'active' => 1),
			'pl' => array('gmap_lat' => '52.198559', 'gmap_lng' => '21.005859', 'region' => 'eu', 'label' => 'Polska', 'active' => 1),
			'hr' => array('gmap_lat' => '45.835796', 'gmap_lng' => '16.018066', 'region' => 'eu', 'label' => 'Hrvatska', 'active' => 0),
			'ga' => array('gmap_lat' => '53.354855', 'gmap_lng' => '-6.260834', 'region' => 'eu', 'label' => 'Ireland', 'active' => 0),
			'bg' => array('gmap_lat' => '42.687309', 'gmap_lng' => '23.323975', 'region' => 'eu', 'label' => 'България', 'active' => 1),
			'fr' => array('gmap_lat' => '48.828255', 'gmap_lng' => '2.331848', 'region' => 'eu', 'label' => 'France', 'active' => 1),
			'no' => array('gmap_lat' => '59.915537', 'gmap_lng' => '10.733643', 'region' => 'eu', 'label' => 'Norge', 'active' => 1),
			'lv' => array('gmap_lat' => '56.943944', 'gmap_lng' => '24.093018', 'region' => 'eu', 'label' => 'Latvija', 'active' => 0),
			'sv' => array('gmap_lat' => '59.275179', 'gmap_lng' => '18.083496', 'region' => 'eu', 'label' => 'Sverige', 'active' => 1),
			'hu' => array('gmap_lat' => '47.485338', 'gmap_lng' => '19.055786', 'region' => 'eu', 'label' => 'Magyarország', 'active' => 0),
			'en' => array('gmap_lat' => '51.511214', 'gmap_lng' => '-0.119824', 'region' => 'eu', 'label' => 'International English', 'active' => 1),
			'de' => array('gmap_lat' => '52.509247', 'gmap_lng' => '13.422546', 'region' => 'eu', 'label' => 'Deutschland', 'active' => 1),
			'nl' => array('gmap_lat' => '52.364555', 'gmap_lng' => '4.902649', 'region' => 'eu', 'label' => 'Nederland', 'active' => 1),
			'ba' => array('gmap_lat' => '43.857616', 'gmap_lng' => '18.374634', 'region' => 'eu', 'label' => 'Bosna i Hercegovina', 'active' => 1),
			'si' => array('gmap_lat' => '46.072903', 'gmap_lng' => '14.521179', 'region' => 'eu', 'label' => 'Slovenija', 'active' => 0),
			'rs' => array('gmap_lat' => '44.791247', 'gmap_lng' => '20.456543', 'region' => 'eu', 'label' => 'Србија', 'active' => 0),
			'dk' => array('gmap_lat' => '55.670857', 'gmap_lng' => '12.513428', 'region' => 'eu', 'label' => 'Danmark', 'active' => 1),
			'it' => array('gmap_lat' => '41.892916', 'gmap_lng' => '12.482520', 'region' => 'eu', 'label' => 'Italia', 'active' => 1),
		),

		'domains' => array(
			'skoda-hyperlapse.dev.symbio' => 'cs',
			'pu170.symbio.cz' => 'en',
			'rapidspaceback.es' => 'es',
			'rapidspaceback.at' => 'at',
			'rapidspaceback.be' => 'be',
			'rapidspaceback.sk' => 'sk',
			'rapidspaceback.cz' => 'cs',
			'rapidspaceback.ch' => 'ch',
			'rapidspaceback.ro' => 'ro',
			'rapidspaceback.pt' => 'pt',
			'rapidspaceback.hr' => 'hr',
			'rapidspaceback.ie' => 'ie',
			'rapidspaceback.bg' => 'bg',
			'rapidspaceback.fr' => 'fr',
			'rapidspaceback.no' => 'no',
			'rapidspaceback.lv' => 'lv',
			'rapidspaceback.it' => 'it',
			'rapidspaceback.se' => 'se',
			'rapidspaceback.hu' => 'hu',
			'rapidspaceback.co.uk' => 'en',
			'rapidspaceback.de' => 'de',
			'rapidspaceback.dk' => 'dk',
			'rapidspaceback.ba' => 'ba',
		),

		'countries' => array(
			'ES' => 'es', 'AT' => 'at', 'BE' => 'be', 'SK' => 'sk', 'CZ' => 'cs', 'CH' => 'ch',
			'RO' => 'ro', 'PT' => 'pt', 'PL' => 'pl', 'HR' => 'hr', 'IE' => 'ie', 'BG' => 'bg',
			'FR' => 'fr', 'NO' => 'no', 'LT' => 'lv', 'SE' => 'sv', 'HU' => 'hu', 'EN' => 'en',
			'GB' => 'en', 'UK' => 'en', 'US' => 'en', 'DE' => 'de', 'NL' => 'nl', 'BA' => 'ba',
			'SI' => 'si', 'RS' => 'rs', 'DK' => 'dk', 'IT' => 'it'
		),
		'regions' => array(
			'eu',
		),
	);

	// --------------------------------------------------------------------------
	// Development configurations
	// --------------------------------------------------------------------------

	$config['test'] = array(
		'debug' => false,
		'fb' => array(
			'appId' => '580330512024201',
			'secret' => 'd23b6b71d51f9fcb3aec85f24fb81504'
		),
		'azureConnection' => 'DefaultEndpointsProtocol=http;AccountName=maoc1;AccountKey=Ht0UNW0qI88oetfwC3snFSUsOnb9hSoOIKGn3EzinIbcHqe7RqL47tqDMMt03RqVU/BuHRYAgP+PuUpH/9D9TQ==',
	);

	$config['dev'] = array(
		'debug' => true,
		'fb' => array(
			'appId' => '506519229441369',
			'secret' => 'ed8692446998652f551bcfd32ed0c008'
		),
		'azureConnection' => 'DefaultEndpointsProtocol=http;AccountName=maoc1;AccountKey=Ht0UNW0qI88oetfwC3snFSUsOnb9hSoOIKGn3EzinIbcHqe7RqL47tqDMMt03RqVU/BuHRYAgP+PuUpH/9D9TQ==',
	);

	$config['dev_rambo'] = array(
		'debug' => true,
		'fb' => array(
			'appId' => '670759026268361',
			'secret' => '38303ea36ccc0e7dfdda5d3e8734ed1a'
		),
		'azureConnection' => 'DefaultEndpointsProtocol=http;AccountName=maoc1;AccountKey=Ht0UNW0qI88oetfwC3snFSUsOnb9hSoOIKGn3EzinIbcHqe7RqL47tqDMMt03RqVU/BuHRYAgP+PuUpH/9D9TQ==',
	);

