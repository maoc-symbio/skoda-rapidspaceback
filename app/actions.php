<?php

	use Symfony\Component\HttpFoundation\Request,
		Symfony\Component\HttpFoundation\Response,
		Silex\Application;

	// --------------------------------------------------------------------------
	// Middleware
	// --------------------------------------------------------------------------

		$resolveApplicationCulture = function (Request $request, Application $app) {

			$cultures = $app['i18n']->getCultures();
			foreach ($cultures as $key=>$c) {
				if ($c['active'] == 0) {
					unset($cultures[$key]);
				}
			}
			if ($request->get('lang', FALSE) && !isset($cultures[$request->get('lang')])) {
				throw new Exception(); // Throws a 404 page
			}

			$app['twig']->addGlobal('availableCultures', $cultures);
			$app['i18n']->resolveCulture($request->get('lang'));
			$app['twig']->addGlobal('i18n', $app['i18n']->getTranslations());
			$app['twig']->addGlobal('culture', $app['i18n']->getCultureInfo());
			$app['twig']->addGlobal('isMobile', $app['mobileDetect']->isMobile());
			$app['twig']->addGlobal('isTablet', $app['mobileDetect']->isTablet());

		};

	// --------------------------------------------------------------------------
	// Main application entry points
	// --------------------------------------------------------------------------

		$app->get('/', function() use ($app)
		{
 			$geocoder = new \Geocoder\Geocoder();
			$adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
			$chain = new \Geocoder\Provider\ChainProvider(array(
				new \Geocoder\Provider\FreeGeoIpProvider($adapter),
				new \Geocoder\Provider\HostIpProvider($adapter),
			));
			$geocoder->registerProvider($chain);

			$country = $geocoder->geocode($_SERVER['REMOTE_ADDR']);
			$countryList = $app['i18n']->getCountries();

			if (!empty($country['countryCode']) && isset($countryList[$country['countryCode']])) {
				return $app->redirect('/' . $countryList[$country['countryCode']]);
			}
			return $app->redirect('/' . $app['i18n']->getCulture());

		})->before($resolveApplicationCulture);

		// Homepage entry point
		$app->get('/{lang}', function($lang) use ($app)
		{
			if ($app['mobileDetect']->isMobile() && !$app['mobileDetect']->isTablet()) {
				return $app->redirect('/' . $app['i18n']->getCulture() . '/m/home');
			}
			$template = ($app['mobileDetect']->isTablet() ? 'application-tablet.html.twig' : 'application.html.twig');
			return $app['twig']->render($template, array(
				'isRide' => false,
				'statistics' => $app['statsManager']->getStatistics($app['i18n']->getCultureInfo())
			));
		})
		->assert('lang', '([a-z]{2})([a-z\-]{3})?')
		->before($resolveApplicationCulture);

		// Ride detail
		$app->get('/{lang}/ride/{hash}', function($lang, $hash) use ($app)
		{
			$ride = $app['rideManager']->findRideByHash($hash);
			if (!$ride) {
				return new Response($app['twig']->render('404.html.twig'), 404);
			}

			return $app['twig']->render('application.html.twig', array(
				'hash' => $hash,
				'ride' => array(
					'points' => json_encode(unserialize($ride->getPropertyValue('points'))),
					'target' => json_encode(unserialize($ride->getPropertyValue('target'))),
					'speed' => $ride->getPropertyValue('speed'),
					'effect' => $ride->getPropertyValue('effect'),
					'pitch' => $ride->getPropertyValue('pitch'),
					'hasMorePoints' => count(unserialize($ride->getPropertyValue('points')) > 0),
					'hash' => $ride->getPropertyValue('hash'),
				),
				'isRide' => true,
				'statistics' => $app['statsManager']->getStatistics($app['i18n']->getCultureInfo())
			));

		})
		->assert('lang', '([a-z]{2})([a-z\-]{3})?')
		->assert('hash', '\w{18}')
		->before($resolveApplicationCulture);

		$app->post('/save/ride', function(Request $request) use ($app)
		{
			$ride = $request->get('ride');
			$culture = $request->get('culture');
			$cultures = $app['i18n']->getCultures();
			if (!isset($cultures[$culture])) {
				return json_encode(array("error" => "Unsupported culture " . $culture));
			}

			$app['i18n']->resolveCulture($culture);
			$ci = $app['i18n']->getCultureInfo();

			$result = $app['rideManager']->save(array(
				'name' => $ride['name'],
				'facebookId' => $ride['id'],
				'gender' => $ride['gender'],

				'points' => serialize($ride['points']),
				'target' => serialize($ride['target']),
				'speed' => $ride['speed'],
				'pitch' => $ride['pitch'],
				'effect' => $ride['effectId'],

				'culture' => $ci['country'],
				'length' => $ride['length'],
			), $culture, $cultures[$culture]['region']);

			return $result ? json_encode($result) : "Error";
		});

	// --------------------------------------------------------------------------
	// Mobile
	// --------------------------------------------------------------------------

		$app->get('/{lang}/m/{page}', function($lang, $page) use ($app)
		{
			if (!$app['mobileDetect']->isMobile()) {
				return $app->redirect('/' . $app['i18n']->getCulture());
			}
			$data = array(
				"home" => array(),
				"statistics" => array('statistics' => $app['statsManager']->getStatistics($app['i18n']->getCultureInfo())),
				"journey" => array(),
				"create" => array('r' => (isset($_GET['r']) ? $_GET['r'] : -1)),
				"after-video" => array(),
				"video" => array(),
			);
			return $app['twig']->render('mobile/'.$page.'.html.twig', array_merge(array(
				'bodyClass' => $page, 'r' => -1
			), $data[$page]));
		})
		->assert('lang', '([a-z]{2})([a-z\-]{3})?')
		->assert('page', '(home|statistics|journey|create|after-video|video)')
		->bind('mobile')
		->before($resolveApplicationCulture);

	// --------------------------------------------------------------------------
	// Facebook actions
	// --------------------------------------------------------------------------

		$app->get('/channel.html', function() use ($app)
		{
 			$cache_expire = 60*60*24*365;
 			header("Pragma: public");
 			header("Cache-Control: max-age=".$cache_expire);
 			header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$cache_expire) . ' GMT');
			return '<script src="//connect.facebook.net/en_US/all.js"></script>';
		});

		$app->match('/publish-action/{action}', function($action) use ($app)
		{
			$request = $app['request'];

			$culture = $request->get('culture', 'en');
			$id = $request->get('id', '');

			$actions = array(
				'enjoy' => array(
					'test_drive' => "http://www.rapidspaceback.com/action/enjoy/".$culture."/".$id
				),
				'enter' => array(
					'world_of_skoda_rapid' => 'http://www.rapidspaceback.com/action/enter/'.$culture
				)
			);
			if (!isset($actions[$action])) {
				return 'error';
			}
			try {
				$response = $app['facebook']->publishOGAction($action, $actions[$action]);
				return 'Ok';
			} catch (\Exception $e) {
				return 'Err';
			}
		})->method('GET|POST');

		$app->get('/action/{action}/{culture}/{id}', function($action, $culture, $id) use ($app) {
			$actions = array(
				'enjoy' => array(
					'type' => 'test_drive',
					'action' => 'enjoy',
					'title' => 'My test drive in ŠKODA Rapid Spaceback',
					'culture' => $culture,
					'id' => $id
				),
				'enter' => array(
					'type' => 'world_of_skoda_rapid',
					'action' => 'enter',
					'title' => 'World of Škoda Rapid',
					'culture' => $culture,
					'id' => $id
				)
			);
			if (!isset($actions[$action])) {
				return $app->redirect('/');
			}
			return $app['twig']->render('og-action.html.twig', $actions[$action]);
		})->value('culture', 'en')->value('id', NULL);

		$app->match('/facebook-connect', function() use ($app)
		{
			return json_encode($app['facebook']->getUserFacebookInfo());
		})->method('GET|POST');

	// --------------------------------------------------------------------------
	// AJAX utilities and support
	// --------------------------------------------------------------------------

		$app->get('{lang}/ajax/templates.js', function() use ($app) {
			$templates = $app['twig']->render('js.html.twig', array('translations' => $app['i18n']->getTranslations()));
			$response = new Response($templates, 200, array('Content-type' => 'text/javascript'));
			return $response;
		})
		->assert('lang', '([a-z]{2})([a-z\-]{3})?')
		->before($resolveApplicationCulture);

	// --------------------------------------------------------------------------
	// Errors handler & system stuff
	// --------------------------------------------------------------------------

		$app->error(function(\Exception $e, $code) use ($app)
		{

		    if (!$app['debug'])
		    {
		    	$uri = $app['request']->getUri();
		    	$isHttps = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
		    	$uri = str_replace($isHttps ? "https://" : "http://", "", $uri);
		    	$uri = str_replace($_SERVER['HTTP_HOST'], "", $uri);
		    	$uri = trim($uri, "/");

		    	$cultures = $app['i18n']->getCultures();
				foreach ($cultures as $key=>$c) {
					if ($c['active'] == 0) {
						unset($cultures[$key]);
					}
				}

		    	$uri_segments = explode("/", $uri);
		    	if (preg_match('/([a-z]{2})([a-z]{3})?/', $uri_segments[0], $m) && isset($cultures[$uri_segments[0]])) {
		    		$app['i18n']->resolveCulture($uri_segments[0]);
		    	} else {
		    		$app['i18n']->resolveCulture();
		    	}

				$app['twig']->addGlobal('i18n', $app['i18n']->getTranslations());
				$app['twig']->addGlobal('availableCultures', $app['i18n']->getCultures());
				$app['twig']->addGlobal('culture', $app['i18n']->getCultureInfo());
				$app['twig']->addGlobal('isMobile', $app['mobileDetect']->isMobile());
				$app['twig']->addGlobal('error', true);

				return new Response($app['twig']->render('404.html.twig'), 404);
		    }
		});

		$app->get('/initialize', function() use ($app)
		{
			$app['azure']->initialize(array(
				"cultures" => array_keys($app['i18n']->getCultures()),
				"regions" => $app['i18n']->getRegions(),
			));
			return 'You will be redirected to homepage in 5 seconds.<script>window.setTimeout(function(){location.href = "http://'.$_SERVER['HTTP_HOST'].'";}, 5000);</script>';
		});

		$app->get('/domains', function() use ($app)
		{
			return serialize($app['i18n']->getDomains());
		});
