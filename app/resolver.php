<?php

	/**
	 * Language resolver for domains used as gateway to main application
	 *
	 * @author SYMBIO
	 * @version 1.0
	 * @package Skoda
	 */

	$productionDomain = 'http://rapidspaceback.com';
	$domains = @file_get_contents($productionDomain . '/domains');

	if ($domains && $domains = unserialize($domains))
	{
		$host = $_SERVER['HTTP_HOST'];
		if (isset($domains[$host])) {
			header('Location: ' . $productionDomain . '/' . $domains[$host]);
			exit;
		}
	}

	header('Location: '. $productionDomain . '/');
	exit;