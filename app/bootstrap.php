<?php

	// --------------------------------------------------------------------------
	// Twig stuff
	// --------------------------------------------------------------------------

		$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
		$app->register(new Silex\Provider\TwigServiceProvider(), array(
			'twig.path' => __DIR__.'/views',
			'twig.options' => array('debug' => $app['debug'])
		));
		$app['twig']->addExtension(new Twig_Extensions_Extension_Debug());
		$app['twig']->addFunction(new Twig_SimpleFunction('__t', function($str) use ($app) {
			$globals = $app['twig']->getGlobals();
			$translations = $globals['i18n'];
			if (isset($translations[$str])) {
				return $translations[$str];
			}
			return $str;
		}));

	// --------------------------------------------------------------------------
	// 3rd party libraries
	// --------------------------------------------------------------------------

		$app['mobileDetect'] = new Mobile_Detect();
		$app['facebook'] = new Skoda\Service\FacebookService(new Facebook($app['config']['fb']));
		$app['azure'] = new Skoda\Service\AzureService($app['config']['azureConnection']);

	// --------------------------------------------------------------------------
	// Internal application services
	// --------------------------------------------------------------------------

		$app['i18n'] = new Skoda\Service\i18nService("en", $config['application']);
		$app['statsManager'] = new Skoda\Service\StatsManager($app['azure'], $app['i18n']);
		$app['rideManager'] = new Skoda\Service\RideManager($app['azure'], $app['statsManager']);

	require_once(__DIR__ . '/utils.php');
	require_once(__DIR__ . '/actions.php');